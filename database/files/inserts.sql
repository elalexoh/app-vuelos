INSERT INTO levels (name,description) VALUES
    ('Level 1','Administradores de BHA con control sobre todas las áreas. Ven y editan todo.'),
    ('Level 2','Administradores de BHA con algunas restricciones. Pueden ver y editar todos los inferiores. No pueden ver globales, news y en el futuro la parte contable'),
    ('Level 3','Administradores de BHA que pueden consultar y crear vuelos. Pueden crear admins de todos los niveles inferiores y ver todos los 4, 5 y 6'),
    ('Level 4','Personas o empresas que tienen aeronaves o venden vuelos y están en plataforma. Crean admin level 5, 6 o 7. Solo ven los que ellos crearon'),
    ('Level 5','Personas dependientes de los nivel 4, que trabajan para empresas que tienen aeronaves o venden vuelos'),
    ('Level 6','Crew members o miembros que trabajan para operadores nivel 4 operando sus aeronaves'),
    ('Level 7','Clientes que utilizan fly blue o compran vuelos por otros medios')
;

INSERT INTO continent(code, name) VALUES
    ('AF','Africa'),
    ('AS','Asia'),
    ('EU','Europe'),
    ('NA','North America'),
    ('OC','Oceania'),
    ('SA','South America'),
;

INSERT INTO countrie(id,code, name, id_continent) VALUES
    (1,'AO','Angola',1),
    (2,'AM','Armenia',2),
    (3,'AT','Austria',3),
    (4,'AW','Aruba',4),
    (5,'AS','American Samoa',5),
    (6,'AR','Argentina',6)
;

INSERT INTO company(company_name, company_position, company_website, id_countrie) VALUES
    ('Aruba Networks','IT','https://www.arubanetworks.com/es/',4),
    ('ARCOR','CEO','https://www.arcor.com/ar',6)
;

INSERT INTO passport(number_passport, due_date, passenger_picture, passport_picture_1, passport_picture_2, id_countrie) VALUES
    (123652361,'2022-12-03','http://localstorage-user=4545fds','http://localstorage-user=243df','http://localstorage-user=234ghj897',2),
    (985982323,'2022-07-22','http://localstoe-user=djhshsf5fds','http://lostorage-user=2dfsa33df','http://locarage-userdsdfshj897',3)
;

INSERT INTO personal_information(
        first_name, last_name, date_of_birth,
        personal_email,personal_phone,representative_first_name,
        representative_last_name,representative_email,
        representative_phone,id_company,id_passport,id_countrie,
        gender, language
    )
    VALUES
        ('Ricardo','Perez','1998-11-26','rperezjo@gmail.com','4242124345','Rjsdjas','Poleljfd','ricardo-perez@jirehpro.com','237654365',1,1,2,'M','Spanish'),
        ('Angel','Castillo','1998-03-13','angel@gmail.com','74365456','Anfuhduh','Cagsdfg','angel-castillo@jirehpro.com','98754323',2,2,3,'M','English')
;

INSERT INTO users (username, password, created_date, id_level, id_personal_information) VALUES
    ('rperezjo@gmail.com','4813494d137e1631bba301d5acab6e7bb7aa74ce1185d456565ef51d737677b2','2021-01-20',1,1),
    ('mambelangel5@gmail.com','4813494d137e1631bba301d5acab6e7bb7aa74ce1185d456565ef51d737677b2','2021-01-20',1,2)
;

INSERT INTO type_of_operation (name) VALUES
    ('Premium Aircraft'),
    ('Medium Aircraft')
;

INSERT INTO aircraft_model (id,name, manufacter, capacity) VALUES
    (23443324,'Dornier 328JET','328 SUPPORT SERVICES',14),
    (34534545,'450 Ultra','3XTRIM',10)
;

INSERT INTO airport (id	, ident , type, name ) VALUES
    (23432423,'00A','heliport','Total Rf Heliport'),
    (32342361,'00AA','small_airport','Aero B Ranch Airport')
;

INSERT INTO aircraft (certification	, id_user_owner , id_aircraft_model, id_airport, id_type_operation ) VALUES
    ('91',1,23443324,23432423,1),
    ('Militar',2,34534545,32342361,2)
;
