DROP DATABASE IF EXISTS appvuelos_jirehpro;

CREATE DATABASE IF NOT EXISTS appvuelos_jirehpro;
USE appvuelos_jirehpro;

CREATE TABLE `levels`(
    id int(255) AUTO_INCREMENT NOT NULL,
    name varchar(56) NOT NULL,
    description text,
    CONSTRAINT pk_levels PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `crewmember_detail`(
    id int(255) AUTO_INCREMENT NOT NULL,
    license_type varchar(56) NOT NULL,
    retings int(255) NOT NULL,
    CONSTRAINT pk_crew_details PRIMARY KEY (id)
)ENGINE=InnoDB;


CREATE TABLE `continent`(
    id int(255) AUTO_INCREMENT NOT NULL,
    code varchar(56) NOT NULL,
    name varchar(64) NOT NULL,
    CONSTRAINT pk_continent PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `countrie`(
    id int(255) UNIQUE NOT NULL,
    code varchar(56) NOT NULL,
    name varchar(128) NOT NULL,
    continent varchar(128),
    wikipedia_link varchar(255),
    keywords varchar(255),
    id_continent int(255),
    countrie_flag varchar(255),
    CONSTRAINT id_continent_countrie FOREIGN KEY (id_continent) REFERENCES continent(id),
    CONSTRAINT pk_countrie PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `region`(
    id int(255) UNIQUE NOT NULL,
    code varchar(56) NOT NULL,
    local_code varchar(56) NOT NULL,
    name varchar(156) NOT NULL,
    continent varchar(156),
    iso_country varchar(156),
    wikipedia_link varchar(255),
    keywords varchar(156),
    img varchar(255),
    id_countrie int(255),
    CONSTRAINT id_countrie_region FOREIGN KEY (id_countrie) REFERENCES countrie(id),
    CONSTRAINT pk_region PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `passport`(
    id int(255) AUTO_INCREMENT NOT NULL,
    number_passport int(255) NOT NULL,
    due_date date NOT NULL,
    passenger_picture varchar(255),
    passport_picture_1 varchar(255),
    passport_picture_2 varchar(255),
    passport_picture_3 varchar(255),
    passport_picture_4 varchar(255),
    id_countrie int(255) NOT NULL,
    CONSTRAINT id_countrie_passport FOREIGN KEY (id_countrie) REFERENCES countrie(id),
    CONSTRAINT pk_passport PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `company`(
    id int(255) AUTO_INCREMENT NOT NULL,
    company_name varchar(255) NOT NULL,
    company_position varchar(255) NOT NULL,
    company_website varchar(255) NOT NULL,
    id_countrie int(255) NOT NULL,
    CONSTRAINT id_countrie_company FOREIGN KEY (id_countrie) REFERENCES countrie(id),
    CONSTRAINT pk_company PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `personal_information`(
    id int(255) AUTO_INCREMENT NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    date_of_birth date NOT NULL,
    personal_email varchar(255) NOT NULL,
    personal_phone varchar(128) NOT NULL,
    representative_first_name varchar(255),
    representative_last_name varchar(255),
    representative_email varchar(255),
    representative_phone varchar(255),
    gender varchar(255),
    language varchar(255),
    admin_notes text,
    flown_km varchar(128),
    paid_usd numeric(8,2),
    id_company int(255),
    id_passport int(255),
    id_countrie int(255),
    CONSTRAINT id_countrie_personal_information FOREIGN KEY (id_countrie) REFERENCES countrie(id),
    CONSTRAINT id_passport_personal_information FOREIGN KEY (id_passport) REFERENCES passport(id),
    CONSTRAINT id_company_personal_information FOREIGN KEY (id_company) REFERENCES company(id),
    CONSTRAINT pk_personal_information PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `users`(
    id int(255) AUTO_INCREMENT NOT NULL,
    username varchar(255) UNIQUE NOT NULL,
    password varchar(255) NOT NULL,
    created_date datetime NOT NULL,
    modified_date datetime,
    id_personal_information int(255),
    id_crewmember_detail int(255),
    id_level int(255) NOT NULL,
    id_user_creator int(255),
    CONSTRAINT id_personal_users FOREIGN KEY (id_personal_information) REFERENCES personal_information(id),
    CONSTRAINT id_crewmember_users FOREIGN KEY (id_crewmember_detail) REFERENCES crewmember_detail(id),
    CONSTRAINT id_level_users FOREIGN KEY (id_level) REFERENCES levels(id),
    CONSTRAINT id_user_users FOREIGN KEY (id_user_creator) REFERENCES users(id),
    CONSTRAINT pk_users PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `reset_pass`(
    id int(255) AUTO_INCREMENT NOT NULL,
    token varchar(255) NOT NULL,
    date_reset datetime NOT NULL,
    id_user int(255) NOT NULL,
    CONSTRAINT id_user_reset_pass FOREIGN KEY (id_user) REFERENCES users(id),
    CONSTRAINT pk_resetpass PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `forum`(
    id int(255) AUTO_INCREMENT NOT NULL,
    subject varchar(255) NOT NULL,
    description text NOT NULL,
    id_user int(255) NOT NULL,
    CONSTRAINT id_user_forum FOREIGN KEY (id_user) REFERENCES users(id),
    CONSTRAINT pk_forum PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `quote`(
    id int(255) AUTO_INCREMENT NOT NULL,
    quote_date datetime NOT NULL,
    quote_type varchar(255) NOT NULL,
    depart_date date NOT NULL,
    number_passengers int(255),
    id_region_depart int(255) NOT NULL,
    id_user int(255) NOT NULL,
    CONSTRAINT id_user_quote FOREIGN KEY (id_user) REFERENCES users(id),
    CONSTRAINT id_region_depart_quote FOREIGN KEY (id_region_depart) REFERENCES region(id),
    CONSTRAINT pk_quote PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `quote_arrive`(
    arrive_date int(255) NOT NULL,
    id_quote int(255) NOT NULL,
    id_region int(255) NOT NULL,
    CONSTRAINT id_region_quote_arrive FOREIGN KEY (id_region) REFERENCES region(id),
    CONSTRAINT id_quote_quote_arrive FOREIGN KEY (id_quote) REFERENCES quote(id),
    CONSTRAINT pk_quote_arrive PRIMARY KEY (id_quote, id_region)
)ENGINE=InnoDB;

CREATE TABLE `type_aircraft_group`(
    id int(255) AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    CONSTRAINT pk_type_of_aircraft_group PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `aircraft_model`(
    id int(255) UNIQUE NOT NULL ,
    name varchar(255) NOT NULL,
    manufacter varchar(255) NOT NULL,
    range_value double NOT NULL,
    capacity int(255),
    type_designator varchar(255),
    description varchar(255),
    engine_type varchar(255),
    aircraft_group int(255),
    engine_count int(255),
    wtc varchar(255),
    shown_name varchar(255),
    max_range_nm double,
    max_elev_isa double,
    avg_speed double,
    required_rwy_sfc double,
    required_rwy_width double,
    required_rwy_lenght double,
    side_picture varchar(255),
    default_picture1 varchar(255),
    default_picture2 varchar(255),
    id_aircraft_group int(255),
    CONSTRAINT id_aircraft_group_aircraft_model FOREIGN KEY (id_aircraft_group) REFERENCES type_of_aircraft_group(id),
    CONSTRAINT pk_aircraft_model PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `aircraft_cost_info`(
    id int(255) AUTO_INCREMENT NOT NULL,
    has_segmented_prices int(255) NOT NULL, -- 1 or 0
    has_distance_prices int(255) NOT NULL, -- 1 or 0
    unit_used_km int(255) NOT NULL, -- para ver si unidad son KM o NM
    min_dist double NOT NULL,
    price_1 int(255),
    price_2 int(255),
    price_3 int(255),
    price_4 int(255),
    price_5 int(255),
    distance_th_1 double,
    distance_th_2 double,
    distance_th_3 double,
    distance_th_4 double,
    medical_cost_1 double,
    medical_cost_2 double,
    medical_cost_th double,
    min_trip_lenght double,
    min_trip_price double,
    national_taxes double,
    national_layovers_cost double,
    cabincrew_cost double,
    avg_speed_1 double,
    avg_speed_2 double,
    avg_speed_3 double,
    avg_speed_4 double,
    avg_speed_5 double,
    tv_max int(255),
    psv_max int(255),
    CONSTRAINT pk_aircraft_cost_info PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `type_operation`(
    id int(255) AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    CONSTRAINT pk_type_of_operation PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `airport`(
    id int(255) AUTO_INCREMENT NOT NULL,
    ident varchar(255) NOT NULL,
    type int(255),
    name varchar(255),
    latitude_deg varchar(255),
    longitude_deg varchar(255),
    elevation_ft varchar(255),
    continent varchar(255),
    iso_country varchar(255),
    iso_region varchar(255),
    municipality varchar(255),
    scheduled_service int(255),
    gps_code varchar(255),
    iata_code varchar(255),
    local_code varchar(255),
    home_link varchar(255),
    wikipedia_link varchar(255),
    keywords varchar(255),
    id_region int(255),
    CONSTRAINT id_region_airport FOREIGN KEY(id_region) REFERENCES region(id),
    CONSTRAINT pk_aircraft PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE `cabecera`(
    id int(255) AUTO_INCREMENT NOT NULL,
    le_ident varchar(255) NOT NULL,
    le_latitude_deg varchar(255),
    le_longitude_deg varchar(255),
    le_elevation_ft varchar(255),
    le_heading_degT varchar(255),
    le_displaced_threshold_ft varchar(255),
    he_ident varchar(255),
    he_latitude_deg varchar(255),
    he_longitude_deg varchar(255),
    he_elevation_ft varchar(255),
    he_heading_degT varchar(255),
    he_displaced_threshold_ft varchar(255),
    CONSTRAINT pk_cabecera PRIMARY KEY(id)
)ENGINE=InnoDB;

CREATE TABLE `airport_rwy`(
    id int(255) AUTO_INCREMENT NOT NULL,
    lenght_ft double NOT NULL,
    width_ft double NOT NULL,
    surface varchar(255) NOT NULL,
    lighted int(255),
    closed int(255),
    id_airport int(255) NOT NULL,
    id_cabecera int(255) NOT NULL,
    CONSTRAINT id_cabecera_airport_rwy FOREIGN KEY(id_cabecera) REFERENCES cabecera(id),
    CONSTRAINT id_airport_airport_rwy FOREIGN KEY(id_airport) REFERENCES airport(id),
    CONSTRAINT pk_airport_rwy PRIMARY KEY(id)
)ENGINE=InnoDB;

CREATE TABLE `aircraft_detail`(
    id int(255) AUTO_INCREMENT NOT NULL,
    picture_main varchar(255) NOT NULL,
    picture_secundary_1 varchar(255) NOT NULL,
    picture_secundary_2 varchar(255),
    aircraft_priority int(255),
    owner_name varchar(255),
    operator_name varchar(255),
    tail_number varchar(255),
    aircraft_yom double,
    user_notes text,
    admin_notes text,
    last_update datetime,
    calification double,
    CONSTRAINT pk_aircraft_detail PRIMARY KEY(id)
)ENGINE=InnoDB;

CREATE TABLE `aircraft_service_level`(
    id int(255) AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    icon varchar(255),
    detail text,
    CONSTRAINT pk_aircraft_service_level PRIMARY KEY(id)
)ENGINE=InnoDB;

CREATE TABLE `aircraft_detail_service`(
    id_aircraft int(255) NOT NULL,
    id_service int(255) NOT NULL,
    CONSTRAINT id_aircraft_detail_service FOREIGN KEY (id_aircraft) REFERENCES aircraft_detail(id),
    CONSTRAINT id_service_detail_service FOREIGN KEY (id_service) REFERENCES aircraft_service_level(id),
    CONSTRAINT pk_aircraft_detail_service PRIMARY KEY (id_aircraft, id_service)
)ENGINE=InnoDB;

CREATE TABLE `aircraft`(
    id int(255) AUTO_INCREMENT NOT NULL,
    certification varchar(255) NOT NULL,
    available int(255),
    showResults int(255),
    pending_aproval int(255),
    id_user_owner int(255),
    id_type_operation int(255),
    id_aircraft_model int(255),
    id_airport int(255),
    id_aircraft_detail int(255),
    id_aircraft_cost_info int(255),
    CONSTRAINT id_user_owner_aircraft FOREIGN KEY (id_user_owner) REFERENCES users(id),
    CONSTRAINT id_aircraft_detail_aircraft FOREIGN KEY (id_aircraft_detail) REFERENCES aircraft_detail(id),
    CONSTRAINT id_aircraft_cost_info_aircraft  FOREIGN KEY (id_aircraft_cost_info) REFERENCES aircraft_cost_info(id),
    CONSTRAINT id_type_operation_aircraft FOREIGN KEY (id_type_operation) REFERENCES type_of_operation(id),
    CONSTRAINT id_aircraft_model_aircraft FOREIGN KEY (id_aircraft_model) REFERENCES aircraft_model(id),
    CONSTRAINT id_airport_aircraft FOREIGN KEY (id_airport) REFERENCES airport(id),
    CONSTRAINT pk_aircraft PRIMARY KEY (id)
)ENGINE=InnoDB;




