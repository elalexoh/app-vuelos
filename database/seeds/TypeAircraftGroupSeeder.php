<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeAircraftGroupSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Single engine",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Multi engine",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Turbo prop single engine",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Turbo prop",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Light jets",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Mid jets",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Super mid jets",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Large jets",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Airliners",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Widebody Airliners",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Helicopters",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Helicopter (Twin engine)",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Gliders",
      ]
    );
    DB::table('type_aircraft_group')->insert(
      [
        'name'        => "Others",
      ]
    );
  }
}
