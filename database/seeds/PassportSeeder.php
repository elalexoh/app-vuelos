<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PassportSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('passport')->insert(
      [
        'number' => 123652361,
        'due_date' => '2022-12-03',
        'picture' => 'http://placehold.it/500?text=appvuelo',
        'picture_1' => 'http://placehold.it/500?text=appvuelo',
        'picture_2' => 'http://placehold.it/500?text=appvuelo',
        'picture_3' => 'http://placehold.it/500?text=appvuelo',
        'picture_4' => 'http://placehold.it/500?text=appvuelo',
        'id_country' => 302789,
      ]
    );
    DB::table('passport')->insert(
      [
        'number' => 985982323,
        'due_date' => '2022-07-22',
        'picture' => 'http://placehold.it/500?text=appvuelo',
        'picture_1' => 'http://placehold.it/500?text=appvuelo',
        'picture_2' => 'http://placehold.it/500?text=appvuelo',
        'picture_3' => 'http://placehold.it/500?text=appvuelo',
        'picture_4' => 'http://placehold.it/500?text=appvuelo',
        'id_country' => 302793,
      ]
    );
  }
}
