<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LegTypesSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('leg_types')->insert(
      [
        'name' => 'Scheduled flight',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Non scheduled flight',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Ferry',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Simulator',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Ground',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Available to fly',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Deadhead',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Office',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Pending assignament',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'On Call',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Day off',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Holidays',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Missed activity',
      ]
    );
    DB::table('leg_types')->insert(
      [
        'name' => 'Other',
      ]
    );
  }
}
