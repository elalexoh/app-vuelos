<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlightsDetailSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('flight_details')->insert(
      [
        'flight_owner'            => 1,
        'allow_contacts_payments' => true,
        'allow_contacts_editions' => true,
        'share_link'              => 'http://placehold.it/250',
        'patients'                => 20,
        'id_calification'         => null,
        'id_quoted_aircraft'      => null,
        // 'id_requested_legs'       => null,
        'id_seller'               => null,
        'id_booking_pax'          => null,
        'id_flight'               => 1
      ]
    );
  }
}
