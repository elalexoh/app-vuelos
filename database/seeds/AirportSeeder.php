<?php

use App\Imports\AirportImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Seeder;

class AirportSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Excel::import(new AirportImport, 'app/airport.csv', null, \Maatwebsite\Excel\Excel::CSV);
  }
}
