<?php

use App\Imports\Airport_rwyImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Seeder;

class AirportRwySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Excel::import(new Airport_rwyImport, 'app/runways.csv', null, \Maatwebsite\Excel\Excel::CSV);
  }
}
