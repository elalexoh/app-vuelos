<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlightsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('flights')->insert(
      [
        'flight_date' => '2020-03-29',
        'flight_condition' => 1,
        'calculated_legs' => 50, //cantidad de asientos disponibles
        'caculated_flight_time' => 60, //
        'entry_status' => 1,
        'id_departure_airport' => 3,
        'id_arrival_airport' => 2,
        'id_aircraft' => 1,
      ]
    );
    DB::table('flights')->insert(
      [
        'flight_date' => '2020-03-29',
        'flight_condition' => 1,
        'calculated_legs' => 25, //cantidad de asientos disponibles
        'caculated_flight_time' => 60, //
        'entry_status' => 0,
        'id_departure_airport' => 3,
        'id_arrival_airport' => 2,
        'id_aircraft' => 2,
      ]
    );
    DB::table('flights')->insert(
      [
        'flight_date' => '2020-03-29',
        'flight_condition' => 1,
        'calculated_legs' => 50, //cantidad de asientos disponibles
        'caculated_flight_time' => 60, //
        'entry_status' => 1,
        'id_departure_airport' => 1,
        'id_arrival_airport' => 2,
        'id_aircraft' => 1,
      ]
    );
  }
}
