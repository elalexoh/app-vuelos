<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AircraftDetailSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('aircraft_detail')->insert(
      [
        'picture_main'            => 'http://placehold.it/250',
        'picture_secundary_1'     => 'http://placehold.it/250',
        'picture_secundary_2'     => 'http://placehold.it/250',
        'aircraft_priority'       => 100,
        'owner_name'              => 'John Doe',
        'operator_name'           => 'Jimmy Doe',
        'tail_number'             => 'prueba',
        'aircraft_yom'            => 2000,
        'user_notes'              => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque maiores nihil tenetur laborum recusandae adipisci quis libero numquam ex, molestias sunt autem corporis tempora suscipit dignissimos obcaecati dicta! Dolorum, voluptates.',
        'admin_notes'             => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque maiores nihil tenetur laborum recusandae adipisci quis libero numquam ex, molestias sunt autem corporis tempora suscipit dignissimos obcaecati dicta! Dolorum, voluptates.',
        'last_update'             => '2020-03-29',
        'calification'            => 100,

      ]
    );
    DB::table('aircraft_detail')->insert(
      [
        'picture_main'            => 'http://placehold.it/250',
        'picture_secundary_1'     => 'http://placehold.it/250',
        'picture_secundary_2'     => 'http://placehold.it/250',
        'aircraft_priority'       => 100,
        'owner_name'              => 'Jane Doe',
        'operator_name'           => 'Jimmy Doe',
        'tail_number'             => 'prueba',
        'aircraft_yom'            => 2000,
        'user_notes'              => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque maiores nihil tenetur laborum recusandae adipisci quis libero numquam ex, molestias sunt autem corporis tempora suscipit dignissimos obcaecati dicta! Dolorum, voluptates.',
        'admin_notes'             => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque maiores nihil tenetur laborum recusandae adipisci quis libero numquam ex, molestias sunt autem corporis tempora suscipit dignissimos obcaecati dicta! Dolorum, voluptates.',
        'last_update'             => '2020-03-29',
        'calification'            => 100,

      ]
    );
  }
}
