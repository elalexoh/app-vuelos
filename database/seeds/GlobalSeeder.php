<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GlobalSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('globals')->insert(
      [
        // 'limite_valor_longRange'        => 1.5,
        // 'coeficiente_correcion_rango'   => 1.5,
        // 'limite_rango_busquedaLR'       => 1.5,
        // 'umbral_LR'                     => 1.5,
        // 'limite_rango_busquedaSR'       => 1.5,
        // 'RANGE'                         => 1000,
        //necesarios para custom calculated
        'TECHNICAL_STOPS'                       => 1,     //technical stops
        'STOPS_AIRCRAFT_RANGE'                  => 1,     //stopsForAircraftWithinRange
        'RANGE_CORRECTION'                      => 0.9,   //coeficiente_correccion_rango
        'DISTANCE_1'                            => 120,   //calculateTotalLayOverCost constant
        'RATE_REMAINING_DAYS'                   => 0.01, //tarifaDiasRestantes
        'RATE_COEFFICIENT'                      => 1.1,   //RATE_COEFICIENT
        'FIXED_VALUE'                           => 200,
        'FIXED_VALUE_2'                         => 300,
        'FIXED_VALUE_3'                         => 400,
        'SUPERCHARGE_FACTOR_CORRECTION'         => 0.1,
        'DISTANCE_CORRECTION_COEFFICIENT'       => 1,   //coeficiente_correcion_distancia
        'DYNAMIC_RATE_COEFICIENT_RATE_LEVEL_2'  => 2,   //coeficiente_correcion_distancia
        'DYNAMIC_RATE_COEFICIENT_RATE_LEVEL_3'  => 0,   //coeficiente_correcion_distancia
        'ELEVATION_CORRECTION_COEFICIENT'       => 1,   //coeficiente_correcion_elevacion
        'MARGIN_RANGE'                          => 75, //coeficiente_correcion_elevacion
        'SEARCH_RANGE_1'                        => 100, //Rango de busqueda 1
      ]
    );
  }
}
