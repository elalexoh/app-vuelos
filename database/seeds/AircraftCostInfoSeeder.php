<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AircraftCostInfoSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('aircraft_cost_info')->insert(
      [
        'has_segmented_prices' => true,
        'has_distance_prices' => true,
        'unit_used_km' => true,
        'min_distance' => 1000,
        'price_1' => 1000,
        'price_2' => 1200,
        'price_3' => 1400,
        'price_4' => 1500,
        'price_5' => 1600,
        'distance_th_1' => 500,
        'distance_th_2' => 600,
        'distance_th_3' => 700,
        'distance_th_4' => 800,
        'distance_th_5' => 900,
        'medical_cost_1' => 1500,
        'medical_cost_2' => 2500,
        'medical_cost_th' => 2000,
        'min_trip_lenght' => 800,
        'min_trip_price' => 3000,
        'national_taxes' => 200,
        'international_taxes' => 300,
        'national_layovers_cost' => 1000,
        'international_layovers_cost' => 1000,
        'international_taxes' => 300,
        'cabincrew_cost' => 250,
        'avg_speed_1' => 200,
        'avg_speed_2' => 300,
        'avg_speed_3' => 400,
        'avg_speed_4' => 500,
        'avg_speed_5' => 600,
        'tv_max' => 5,
        'psv_max' => 10,
      ]
    );
    DB::table('aircraft_cost_info')->insert(
      [
        'has_segmented_prices' => true,
        'has_distance_prices' => true,
        'unit_used_km' => false,
        'min_distance' => 5000,
        'price_1' => 5000,
        'price_2' => 5200,
        'price_3' => 5400,
        'price_4' => 5500,
        'price_5' => 5600,
        'distance_th_1' => 500,
        'distance_th_2' => 500,
        'distance_th_3' => 500,
        'distance_th_4' => 500,
        'distance_th_5' => 500,
        'medical_cost_1' => 5500,
        'medical_cost_2' => 5500,
        'medical_cost_th' => 5000,
        'min_trip_lenght' => 700,
        'min_trip_price' => 5000,
        'national_taxes' => 500,
        'international_taxes' => 500,
        'national_layovers_cost' => 5000,
        'international_layovers_cost' => 5000,
        'international_taxes' => 500,
        'cabincrew_cost' => 550,
        'avg_speed_1' => 500,
        'avg_speed_2' => 500,
        'avg_speed_3' => 500,
        'avg_speed_4' => 500,
        'avg_speed_5' => 500,
        'tv_max' => 5,
        'psv_max' => 10,
      ]
    );
  }
}
