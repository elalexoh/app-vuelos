<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->insert(
      [
        'username' => 'level1@gmail.com',
        'password' => Hash::make('root'),
        'created_date' => '2021-01-20',
        'id_level' => 1,
        'id_personal_information' => 1,
      ]
    );
    DB::table('users')->insert(
      [
        'username' => 'level2@gmail.com',
        'password' => Hash::make('root'),
        'created_date' => '2021-01-20',
        'id_level' => 2,
        'id_personal_information' => 2,
      ]
    );
    DB::table('users')->insert(
      [
        'username' => 'level3@gmail.com',
        'password' => Hash::make('root'),
        'created_date' => '2021-01-20',
        'id_level' => 3,
        'id_personal_information' => 2,
      ]
    );
    DB::table('users')->insert(
      [
        'username' => 'level4@gmail.com',
        'password' => Hash::make('root'),
        'created_date' => '2021-01-20',
        'id_level' => 4,
        'id_personal_information' => 2,
      ]
    );
    DB::table('users')->insert(
      [
        'username' => 'level5@gmail.com',
        'password' => Hash::make('root'),
        'created_date' => '2021-01-20',
        'id_level' => 5,
        'id_personal_information' => 2,
      ]
    );
    DB::table('users')->insert(
      [
        'username' => 'level6@gmail.com',
        'password' => Hash::make('root'),
        'created_date' => '2021-01-20',
        'id_level' => 6,
        'id_personal_information' => 2,
        'id_crewmember_detail' => 1,
      ]
    );
    DB::table('users')->insert(
      [
        'username' => 'level7@gmail.com',
        'password' => Hash::make('root'),
        'created_date' => '2021-01-20',
        'id_level' => 7,
        'id_personal_information' => 2,
        'id_user_creator' => 4,
      ]
    );
  }
}
