<?php

use App\Imports\RegionImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Excel::import(new RegionImport, 'app/regions.csv', null, \Maatwebsite\Excel\Excel::CSV);
  }
}
