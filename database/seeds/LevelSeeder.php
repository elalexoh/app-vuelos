<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('levels')->insert(
      [
        'name' => 'Level 1',
        'description' => 'Administradores de BHA con control sobre todas las áreas. Ven y editan todo.'
      ]
    );
    DB::table('levels')->insert(
      [
        'name' => 'Level 2',
        'description' => 'Administradores de BHA con algunas restricciones. Pueden ver y editar todos los inferiores. No pueden ver globales, news y en el futuro la parte contable'
      ]
    );
    DB::table('levels')->insert(
      [
        'name' => 'Level 3',
        'description' => 'Administradores de BHA que pueden consultar y crear vuelos. Pueden crear admins de todos los niveles inferiores y ver todos los 4, 5 y 6'
      ]
    );
    DB::table('levels')->insert(
      [
        'name' => 'Level 4',
        'description' => 'Personas o empresas que tienen aeronaves o venden vuelos y están en plataforma. Crean admin level 5, 6 o 7. Solo ven los que ellos crearon'
      ]
    );
    DB::table('levels')->insert(
      [
        'name' => 'Level 5',
        'description' => 'Personas dependientes de los nivel 4, que trabajan para empresas que tienen aeronaves o venden vuelos'
      ]
    );
    DB::table('levels')->insert(
      [
        'name' => 'Level 6',
        'description' => 'Crew members o miembros que trabajan para operadores nivel 4 operando sus aeronaves'
      ]
    );
    DB::table('levels')->insert(
      [
        'name' => 'Level 7',
        'description' => 'Clientes que utilizan fly blue o compran vuelos por otros medios'
      ]
    );
  }
}
