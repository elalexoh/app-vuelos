<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('company')->insert(
      [
        'name' => 'Aruba Networks',
        'position' => 'IT',
        'website' => 'https://www.arubanetworks.com/es/',
        'id_country' => 302789, //argentina
      ]
    );
    DB::table('company')->insert(
      [
        'name' => 'ARCOR',
        'position' => 'CEO',
        'website' => 'https://www.arcor.com/ar',
        'id_country' => 302793, //colombia
      ]
    );
  }
}
