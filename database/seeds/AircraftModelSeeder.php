<?php

use Illuminate\Database\Seeder;
use App\Imports\Aircraft_ModelImport;
use Maatwebsite\Excel\Facades\Excel;

class AircraftModelSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Excel::import(new Aircraft_ModelImport, 'app/aircraft_model.csv', null, \Maatwebsite\Excel\Excel::CSV);
    Excel::import(new Aircraft_ModelImport, 'app/models.csv', null, \Maatwebsite\Excel\Excel::CSV);
  }
}
