<?php

use App\Imports\CountryImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class CountrySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Excel::import(new CountryImport, 'app/countries.csv', null, \Maatwebsite\Excel\Excel::CSV);
  }
}
