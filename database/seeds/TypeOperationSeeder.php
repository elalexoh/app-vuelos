<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOperationSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('type_operation')->insert(
      [
        'name' => 'Passengers only'
      ]
    );
    DB::table('type_operation')->insert(
      [
        'name' => 'Passengers and medical (1 stretcher)'
      ]
    );
    DB::table('type_operation')->insert(
      [
        'name' => 'Passengers and medical (2 stretchers)'
      ]
    );
    DB::table('type_operation')->insert(
      [
        'name' => 'Medical only (1 stretcher)'
      ]
    );
    DB::table('type_operation')->insert(
      [
        'name' => 'Medical only (2 stretchers)'
      ]
    );
    DB::table('type_operation')->insert(
      [
        'name' => 'Cargo Only'
      ]
    );
    DB::table('type_operation')->insert(
      [
        'name' => 'Other'
      ]
    );
  }
}
