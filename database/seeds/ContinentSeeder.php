<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContinentSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('continent')->insert(
      [
        'code' => 'AF',
        'name' => 'Africa'
      ]
    );
    DB::table('continent')->insert(
      [
        'code' => 'AS',
        'name' => 'Asia'
      ]
    );
    DB::table('continent')->insert(
      [
        'code' => 'EU',
        'name' => 'Europe'
      ]
    );
    DB::table('continent')->insert(
      [
        'code' => 'NA',
        'name' => 'North America'
      ]
    );
    DB::table('continent')->insert(
      [
        'code' => 'OC',
        'name' => 'Oceania'
      ]
    );
    DB::table('continent')->insert(
      [
        'code' => 'SA',
        'name' => 'South America'
      ]
    );
    DB::table('continent')->insert(
      [
        'code' => 'AN',
        'name' => 'Antarctica'
      ]
    );
  }
}
