<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AircraftSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('aircraft')->insert(
      [
        'certification' => '91',
        'available' => 1,
        'show_results' => 1,
        'pending_approval' => 0,
        'id_user_owner' => 1,
        'id_aircraft_model' => 1,
        'id_airport' => 2,
        'id_type_operation' => 1,
        'id_aircraft_detail' => 1,
        'id_aircraft_cost_info' => 1,
        'range' => 1500,
        'capacity' => 5,
      ]
    );
    DB::table('aircraft')->insert(
      [
        'certification' => 'Militar',
        'available' => 1,
        'show_results' => 1,
        'pending_approval' => 0,
        'id_user_owner' => 2,
        'id_aircraft_model' => 1,
        'id_airport' => 3,
        'id_type_operation' => 2,
        'id_aircraft_detail' => 2,
        'id_aircraft_cost_info' => 2,
        'range' => 1500,
        'capacity' => 5,
      ]
    );
  }
}
