<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HiredFlightsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('hired_flights')->insert(
      [
        'date' => '2020-03-29',
        'id_client' => 1,
        'id_seller' => 2,
        'id_flight' => 1,
        'id_book' => 1,
      ]
    );
  }
}
