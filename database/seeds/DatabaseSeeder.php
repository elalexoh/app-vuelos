<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    $this->call(GlobalSeeder::class);
    $this->call(AircraftCostInfoSeeder::class); //dev
    $this->call(AircraftServiceLevelSeeder::class);
    $this->call(TypeAircraftGroupSeeder::class);
    $this->call(CrewMemberDetailSeeder::class); //dev
    $this->call(LevelSeeder::class);
    $this->call(LegTypesSeeder::class);
    $this->call(ContinentSeeder::class);
    $this->call(CountrySeeder::class);

    $this->call(RegionSeeder::class); //las regiones no se casan con los paises directamente

    $this->call(CompanySeeder::class); //Dev
    $this->call(PassportSeeder::class); //dev
    $this->call(PersonalInformationSeeder::class); //dev
    $this->call(UsersSeeder::class); //dev
    $this->call(TypeOperationSeeder::class);
    $this->call(AircraftModelSeeder::class);
    $this->call(AirportSeeder::class);
    $this->call(AirportRwySeeder::class);
    $this->call(AircraftDetailSeeder::class); //dev
    $this->call(AircraftSeeder::class); //dev
    $this->call(ProhibitedAirportsSeeder::class); //dev
    $this->call(FlightsSeeder::class); //dev
    $this->call(FlightsDetailSeeder::class); //dev
    $this->call(FlightsChargerSeeder::class); //dev
    $this->call(FlightsLegSeeder::class); //dev
    $this->call(FlightsPuntuationSeeder::class); //dev
    $this->call(QuoteSeeder::class); //dev
    $this->call(HiredFlightsSeeder::class); //dev
    $this->call(AircraftDetailServiceSeeder::class); ////dev
  }
}
