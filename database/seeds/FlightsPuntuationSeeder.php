<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlightsPuntuationSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('flight_puntuations')->insert(
      [
        'score'       => 500,
        'id_flight'   => 1,
      ]
    );
  }
}
