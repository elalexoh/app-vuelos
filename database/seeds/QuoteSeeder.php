<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuoteSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('quote')->insert(
      [
        'quote_date' => '2020-03-29',
        'quote_type' => 'Tipo 1',
        'depart_date' => '2020-03-29',
        'number_passengers' => 5,
        'id_region_depart' => 306165,
        'id_user' => 1,
      ]
    );
  }
}
