<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlightsChargerSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('flight_charges')->insert(
      [
        'quoted_operator_price'   => 2000,
        'id_flight'               => 1,
      ]
    );
  }
}
