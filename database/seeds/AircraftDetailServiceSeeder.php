<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AircraftDetailServiceSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('aircraft_detail_service')->insert(
      [
        'id_aircraft' => 1,
        'id_service' => 1,
      ]
    );
    DB::table('aircraft_detail_service')->insert(
      [
        'id_aircraft' => 1,
        'id_service' => 2,
      ]
    );
    DB::table('aircraft_detail_service')->insert(
      [
        'id_aircraft' => 2,
        'id_service' => 1,
      ]
    );
    DB::table('aircraft_detail_service')->insert(
      [
        'id_aircraft' => 2,
        'id_service' => 2,
      ]
    );
  }
}
