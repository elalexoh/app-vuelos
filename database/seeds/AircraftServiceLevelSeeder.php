<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AircraftServiceLevelSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "Catering",
        'icon'        => "catering",
        'detail'      => "Catering",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "Toilette",
        'icon'        => "toilette",
        'detail'      => "Toilette",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "Amenities",
        'icon'        => "amenities",
        'detail'      => "Amenities",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "Cabin crew",
        'icon'        => "cabin_crew",
        'detail'      => "Cabin",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "Cabin crew (On demand)",
        'icon'        => "cabin_crew_on_demaind",
        'detail'      => "Cabin",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "free WI-FI on board",
        'icon'        => "free_wifi_on_board",
        'detail'      => "free",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "WI-FI with charge",
        'icon'        => "wifi_with_charge",
        'detail'      => "WI-FI with charge",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "Pets not allowed",
        'icon'        => "pets_not_allowed",
        'detail'      => "Pets not allowed",
      ]
    );
    DB::table('aircraft_service_level')->insert(
      [
        'name'        => "Otros",
        'icon'        => "otros",
        'detail'      => "Otros",
      ]
    );
  }
}
