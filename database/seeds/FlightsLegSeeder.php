<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlightsLegSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('flight_legs')->insert(
      [
        'leg_date'                      => '2020-03-29',
        'rtd'                           => 'PRUEBA',
        'etd'                           => 'PRUEBA',
        'atd'                           => 'PRUEBA',
        'eta'                           => 'PRUEBA',
        'ata'                           => 'PRUEBA',
        'show_up_time'                  => 'PRUEBA',
        'leg_capacity'                  => 200,
        'empty_seat_price'              => 2000,
        'airport_terminal_indications'  => 200,
      ]
    );
  }
}
