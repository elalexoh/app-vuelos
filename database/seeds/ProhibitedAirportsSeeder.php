<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProhibitedAirportsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('prohibited_airports')->insert(
      [
        'id_aircraft' => 1,
        'id_airport' => 2,
      ]
    );
    DB::table('prohibited_airports')->insert(
      [
        'id_aircraft' => 1,
        'id_airport' => 3,
      ]
    );
  }
}
