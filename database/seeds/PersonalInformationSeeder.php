<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonalInformationSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('personal_information')->insert(
      [
        'first_name' => 'John',
        'last_name' => 'Doe',
        'date_of_birth' => '1998-01-01',
        'email' => 'johnDoe@gmail.com',
        'phone' => '1231234567',
        'representative_first_name' => 'John',
        'representative_last_name' => 'Doe',
        'representative_email' => 'JohnDoe@jirehpro.com',
        'representative_phone' => '1231234567',
        'id_company' => 1,
        'id_passport' => 1,
        'id_country' => 302789,
        'gender' => 'M',
        'language' => 'Spanish'
      ]
    );
    DB::table('personal_information')->insert(
      [
        'first_name' => 'Jane',
        'last_name' => 'Doe',
        'date_of_birth' => '1998-01-01',
        'email' => 'JaneDoe@gmail.com',
        'phone' => '1231234567',
        'representative_first_name' => 'Jane',
        'representative_last_name' => 'Doe',
        'representative_email' => 'JaneDoe@jirehpro.com',
        'representative_phone' => '1231234567',
        'id_company' => 1,
        'id_passport' => 1,
        'id_country' => 302793,
        'gender' => 'F',
        'language' => 'Spanish'
      ]
    );
  }
}
