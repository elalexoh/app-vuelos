<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CrewMemberDetailSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('crewmember_detail')->insert(
      [
        'license_type' => 'type 1',
        'ratings' => 20,
      ]
    );
    DB::table('crewmember_detail')->insert(
      [
        'license_type' => 'type 2',
        'ratings' => 20,
      ]
    );
    DB::table('crewmember_detail')->insert(
      [
        'license_type' => 'type 3',
        'ratings' => 20,
      ]
    );
  }
}
