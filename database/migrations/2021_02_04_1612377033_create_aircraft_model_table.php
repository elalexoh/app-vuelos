<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftModelTable extends Migration
{
  public function up()
  {
    Schema::create('aircraft_model', function (Blueprint $table) {
      // id,0
      // manufacturer,1
      // name,2
      // type_designator,3
      // description,4
      // engine type,5
      // engine count,6
      // wtc,7
      // shown_name,8
      // id_aircraft_group,9
      // capacity,10
      // max_range_nm,11
      // average_speed,12
      // required_rwy_sfc,13
      // required_rwy_width,14
      // required_rwy_lenght,15
      // side_picture,16
      // default_picture1,17
      // default_picture2,18

      $table->increments('id');
      $table->string('manufacturer')->nullable();;
      $table->string('name')->nullable();;
      $table->string('type_designator')->nullable();
      $table->string('description')->nullable();
      $table->string('engine_type')->nullable();
      $table->string('engine_count')->nullable();
      $table->string('wtc')->nullable();
      $table->string('shown_name')->nullable();
      $table->integer('id_aircraft_group')->unsigned()->index()->nullable();
      $table->integer('capacity')->nullable();
      $table->double('max_range_nm', 15, 8)->nullable();
      $table->double('avg_speed')->nullable();
      $table->string('required_rwy_sfc')->nullable();
      $table->double('required_rwy_width', 15, 8)->nullable();
      $table->double('required_rwy_lenght', 15, 8)->nullable();
      $table->string('side_picture')->nullable();
      $table->string('default_picture1')->nullable();
      $table->string('default_picture2')->nullable();
      $table->double('range_value')->nullable();
      $table->double('dummy')->nullable();
      $table->double('max_elev_isa')->nullable(); //este campo no se encuentra, debe ser rellanado con un 10000
      $table->foreign('id_aircraft_group')->references('id')->on('type_aircraft_group');
    });
  }

  public function down()
  {
    Schema::dropIfExists('aircraft_model');
  }
}
