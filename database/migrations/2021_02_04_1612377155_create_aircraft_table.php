<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftTable extends Migration
{
  public function up()
  {
    Schema::create('aircraft', function (Blueprint $table) {

      $table->increments('id');
      $table->string('certification');
      $table->boolean('available')->nullable();
      $table->boolean('show_results')->nullable();
      $table->boolean('pending_approval')->nullable();
      $table->integer('capacity')->nullable();
      $table->float('range')->nullable();
      // foreaneas
      $table->integer('id_airport')->unsigned()->index()->nullable();
      $table->integer('id_user_owner')->unsigned()->index();
      $table->integer('id_type_operation')->unsigned()->index();
      $table->integer('id_aircraft_model')->unsigned()->index();
      $table->integer('id_aircraft_detail')->unsigned()->index()->nullable();
      $table->integer('id_aircraft_cost_info')->unsigned()->index()->nullable();
      // creando relacion
      $table->foreign('id_user_owner')->references('id')->on('users');
      $table->foreign('id_aircraft_detail')->references('id')->on('aircraft_detail');
      $table->foreign('id_aircraft_cost_info')->references('id')->on('aircraft_cost_info');
      $table->foreign('id_type_operation')->references('id')->on('type_operation');
      $table->foreign('id_aircraft_model')->references('id')->on('aircraft_model');
      $table->foreign('id_airport')->references('id')->on('airport');
    });
  }

  public function down()
  {
    Schema::dropIfExists('aircraft');
  }
}
