<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionTable extends Migration
{
  public function up()
  {
    Schema::create('region', function (Blueprint $table) {
      // "id" => $row[0],
      // "code" => $row[1],
      // "local_code" => $row[2],
      // "name" => $row[3],
      // "continent" => $row[4],
      // "iso_country" => $row[5],
      // "wikipedia_link" => $row[6],
      // "keywords" => $row[7]

      $table->increments('id');
      $table->string('code')->length(56);
      $table->string('local_code')->length(56);
      $table->string('name')->length(156);
      $table->string('continent')->length(156);
      $table->string('iso_country')->length(156)->nullable();
      $table->string('wikipedia_link')->length(550)->nullable();
      $table->string('keywords')->length(156)->nullable();
      $table->string('img')->nullable();
      $table->integer('id_country')->unsigned()->index()->nullable();
      $table->foreign('id_country')->references('id')->on('country');
    });
  }

  public function down()
  {
    Schema::dropIfExists('region');
  }
}
