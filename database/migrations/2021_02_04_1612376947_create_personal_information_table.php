<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalInformationTable extends Migration
{
  public function up()
  {
    Schema::create('personal_information', function (Blueprint $table) {

      $table->increments('id');
      $table->string('first_name');
      $table->string('last_name');
      $table->date('date_of_birth');
      $table->string('email');
      $table->string('phone')->length(128);
      $table->string('representative_first_name')->nullable();
      $table->string('representative_last_name')->nullable();
      $table->string('representative_email')->nullable();
      $table->string('representative_phone')->nullable();
      $table->string('gender');
      $table->string('language');
      $table->text('admin_notes')->nullable();
      $table->string('flown_km')->nullable();
      $table->integer('paid_usd')->nullable();
      $table->integer('id_company')->unsigned()->index()->nullable();
      $table->integer('id_passport')->unsigned()->index();
      $table->integer('id_country')->unsigned()->index()->nullable();
      $table->timestamps();

      // foraneas
      $table->foreign('id_country')->references('id')->on('country');
      $table->foreign('id_passport')->references('id')->on('passport');
      $table->foreign('id_company')->references('id')->on('company');
    });
  }

  public function down()
  {
    Schema::dropIfExists('personal_information');
  }
}
