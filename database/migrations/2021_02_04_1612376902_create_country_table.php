<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTable extends Migration
{
  public function up()
  {
    Schema::create('country', function (Blueprint $table) {

      // "id
      // ",
      // "code
      // ",
      // "name
      // ",
      // "continent
      // ",
      // "wikipedia_link
      // ",
      // "keywords
      // "

      $table->increments('id');
      $table->string('code');
      $table->string('name');
      // $table->string('continent')->nullable();
      $table->integer('id_continent')->unsigned()->index();
      $table->string('wikipedia_link')->nullable();
      $table->string('keywords')->nullable();
      $table->string('countrie_flag')->nullable();

      $table->foreign('id_continent')->references('id')->on('continent');
    });
  }

  public function down()
  {
    Schema::dropIfExists('country');
  }
}
