<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumTable extends Migration
{
  public function up()
  {
    Schema::create('forum', function (Blueprint $table) {

      $table->increments('id');
      $table->string('subject');
      $table->text('description');
      $table->integer('id_user')->unsigned()->index();
      $table->foreign('id_user')->references('id')->on('users');
    });
  }

  public function down()
  {
    Schema::dropIfExists('forum');
  }
}
