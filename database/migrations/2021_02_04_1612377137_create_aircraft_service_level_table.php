<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftServiceLevelTable extends Migration
{
  public function up()
  {
    Schema::create('aircraft_service_level', function (Blueprint $table) {

      $table->increments('id');
      $table->string('name');
      $table->string('icon');
      $table->text('detail');
    });
  }

  public function down()
  {
    Schema::dropIfExists('aircraft_service_level');
  }
}
