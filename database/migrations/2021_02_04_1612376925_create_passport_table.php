<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassportTable extends Migration
{
  public function up()
  {
    Schema::create('passport', function (Blueprint $table) {

      $table->increments('id');
      $table->integer('number')->length(255);
      $table->date('due_date');
      $table->string('picture')->nullable();
      $table->string('picture_1')->nullable();
      $table->string('picture_2')->nullable();
      $table->string('picture_3')->nullable();
      $table->string('picture_4')->nullable();
      $table->integer('id_country')->unsigned()->index();
      // relaciones
      $table->foreign('id_country')->references('id')->on('country');
    });
  }

  public function down()
  {
    Schema::dropIfExists('passport');
  }
}
