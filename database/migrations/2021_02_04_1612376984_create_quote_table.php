<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuoteTable extends Migration
{
  public function up()
  {
    Schema::create('quote', function (Blueprint $table) {

      $table->increments('id');
      $table->date('quote_date');
      $table->string('quote_type');
      $table->date('depart_date');
      $table->integer('number_passengers');
      $table->integer('id_region_depart')->unsigned()->index();
      $table->integer('id_user')->unsigned()->index();
      $table->foreign('id_user')->references('id')->on('users');
      $table->foreign('id_region_depart')->references('id')->on('region');
    });
  }

  public function down()
  {
    Schema::dropIfExists('quote');
  }
}
