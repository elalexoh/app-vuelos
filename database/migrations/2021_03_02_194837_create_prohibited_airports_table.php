<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProhibitedAirportsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('prohibited_airports', function (Blueprint $table) {

      $table->increments('id');
      // foreaneas
      // $table->integer('id_airport')->unsigned()->index()->nullable();
      $table->integer('id_aircraft')->unsigned()->index()->nullable();
      $table->integer('id_airport')->unsigned()->index()->nullable();
      // creando relacion
      $table->foreign('id_aircraft')->references('id')->on('aircraft');
      $table->foreign('id_airport')->references('id')->on('airport');

      // select 'id_aircraft'
      // from 'prohibited_airports'
      // where 'id_airport' != 'prohibited_airport'

    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('prohibited_airports');
  }
}
