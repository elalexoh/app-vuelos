<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftDetailServiceTable extends Migration
{
  public function up()
  {
    Schema::create('aircraft_detail_service', function (Blueprint $table) {

      $table->increments('id');
      $table->integer('id_aircraft')->unsigned()->index();
      $table->integer('id_service')->unsigned()->index();
      $table->foreign('id_aircraft')->references('id')->on('aircraft_detail');
      $table->foreign('id_service')->references('id')->on('aircraft_service_level');
      // $table->primary(['id_aircraft', 'id_service']);
    });
  }

  public function down()
  {
    Schema::dropIfExists('aircraft_detail_service');
  }
}
