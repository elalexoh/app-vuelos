<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {

      $table->increments('id');
      $table->string('username');
      $table->string('password');
      $table->datetime('created_date');
      $table->datetime('modified_date')->nullable();

      $table->integer('id_personal_information')->unsigned()->index()->nullable();
      $table->integer('id_crewmember_detail')->unsigned()->index()->nullable();
      $table->integer('id_level')->unsigned()->index();
      $table->integer('id_user_creator')->unsigned()->index()->nullable();

      // relaciones
      $table->foreign('id_personal_information')->references('id')->on('personal_information');
      $table->foreign('id_crewmember_detail')->references('id')->on('crewmember_detail');
      $table->foreign('id_level')->references('id')->on('levels');
      $table->foreign('id_user_creator')->references('id')->on('users');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('users');
  }
}
