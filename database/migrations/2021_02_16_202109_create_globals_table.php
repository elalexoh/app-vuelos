<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('globals', function (Blueprint $table) {
      $table->id();
      $table->float('TECHNICAL_STOPS');
      $table->float('STOPS_AIRCRAFT_RANGE');
      $table->float('RANGE_CORRECTION');
      $table->float('DISTANCE_1');
      $table->float('RATE_COEFFICIENT');
      $table->float('DYNAMIC_RATE_COEFICIENT_RATE_LEVEL_2'); //2
      $table->float('DYNAMIC_RATE_COEFICIENT_RATE_LEVEL_3'); //0
      $table->float('RATE_REMAINING_DAYS');
      $table->float('FIXED_VALUE');
      $table->float('FIXED_VALUE_2');
      $table->float('FIXED_VALUE_3');
      $table->float('SUPERCHARGE_FACTOR_CORRECTION');
      $table->float('DISTANCE_CORRECTION_COEFFICIENT');
      $table->float('ELEVATION_CORRECTION_COEFICIENT');
      // $table->float('limite_valor_longRange');
      // $table->float('coeficiente_correcion_rango');
      // $table->float('limite_rango_busquedaLR');
      // $table->float('umbral_LR');
      // $table->float('limite_rango_busquedaSR');
      // $table->float('RANGE');
      // $table->integer('rango_busqueda1');
      // $table->integer('rango_busqueda2');
      // $table->integer('rango_busqueda3');
      // $table->integer('mostrar_categoria_aeronaves_sin_aeropuerto');
      // $table->integer('cantidad_aeronaves_filtradas');
      // $table->integer('umbral_costomedico');
      // $table->integer('technical_stops');
      $table->integer('MARGIN_RANGE');
      $table->integer('SEARCH_RANGE_1'); //rango de busqueda 1
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('globals');
  }
}
