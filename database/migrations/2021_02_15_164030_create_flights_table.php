<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('flights', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('calculated_legs');
      $table->date('flight_date'); //cuando inicia el vuelo
      $table->integer('caculated_flight_time');
      // $table->integer('actual_flight_time');
      // $table->integer('calculated_duty_time');
      // $table->integer('actual_duty_time');
      //? Vuelo cancelado
      $table->boolean('flight_condition');

      //? 6 si es cotización, 5 si pasajero pone pedir info, 4 si el pasajero lo pide por reserva, 3 si el operador confirmo precio y disponibilidad, 2 si es un vuelo asignado, 1 vuelo aceptado por operador.
      $table->integer('entry_status');

      // foraneas
      $table->integer('id_departure_airport')->unsigned()->index();
      $table->integer('id_arrival_airport')->unsigned()->index();
      $table->integer('id_aircraft')->unsigned()->index();

      // Relaciones
      $table->foreign('id_departure_airport')->references('id')->on('airport');
      $table->foreign('id_arrival_airport')->references('id')->on('airport');
      $table->foreign('id_aircraft')->references('id')->on('aircraft');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('flights');
  }
}
