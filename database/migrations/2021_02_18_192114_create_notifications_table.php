<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('notifications', function (Blueprint $table) {
      $table->id();
      $table->string('type');
      $table->string('description');
      $table->boolean('status');
      $table->integer('id_user_transmitter')->unsigned()->index();
      $table->integer('id_user_receiver')->unsigned()->index();
      $table->foreign('id_user_transmitter')->references('id')->on('users');
      $table->foreign('id_user_receiver')->references('id')->on('users');
      $table->timestamps();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('notifications');
  }
}
