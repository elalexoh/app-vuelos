<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResetPassTable extends Migration
{
  public function up()
  {
    Schema::create('reset_pass', function (Blueprint $table) {

      $table->increments('id');
      $table->string('token');
      $table->datetime('date');
      $table->integer('id_user')->unsigned()->index();
      $table->foreign('id_user')->references('id')->on('users');
    });
  }

  public function down()
  {
    Schema::dropIfExists('reset_pass');
  }
}
