<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCabeceraTable extends Migration
{
  public function up()
  {
    Schema::create('header', function (Blueprint $table) {

      $table->increments('id');
      $table->string('le_ident');
      $table->string('le_latitude_deg');
      $table->string('le_longitude_deg');
      $table->string('le_elevation_ft');
      $table->string('le_heading_degT');
      $table->string('le_displaced_threshold_ft');
      $table->string('he_ident');
      $table->string('he_latitude_deg');
      $table->string('he_longitude_deg');
      $table->string('he_elevation_ft');
      $table->string('he_heading_degT');
      $table->string('he_displaced_threshold_ft');
    });
  }

  public function down()
  {
    Schema::dropIfExists('header');
  }
}
