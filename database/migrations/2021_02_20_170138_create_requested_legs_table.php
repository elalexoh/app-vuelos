<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestedLegsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('requested_legs', function (Blueprint $table) {
      $table->increments('id');
      $table->date('date');
      $table->integer('id_flight_detail')->unsigned()->index()->nullable();
      $table->integer('id_flight_leg')->unsigned()->index()->nullable();
      $table->integer('id_user')->unsigned()->index()->nullable();

      // relaciones
      $table->foreign('id_flight_detail')->references('id')->on('flight_details');
      $table->foreign('id_flight_leg')->references('id')->on('flight_legs');
      $table->foreign('id_user')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('requested_legs');
  }
}
