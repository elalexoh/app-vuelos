<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportRwyTable extends Migration
{
  public function up()
  {
    Schema::create('airport_rwy', function (Blueprint $table) {
      $table->increments('id');
      $table->double('lenght_ft')->nullable();
      $table->double('width_ft')->nullable();
      $table->string('surface')->nullable();
      $table->integer('lighted')->nullable();
      $table->integer('closed')->nullable();
      $table->string('le_ident')->nullable();
      $table->double('le_latitude_deg', 15, 8)->nullable();
      $table->double('le_longitude_deg', 15, 8)->nullable();
      $table->double('le_elevation_ft', 15, 8)->nullable();
      $table->double('le_heading_degT', 15, 8)->nullable();
      $table->double('le_displaced_threshold_ft', 15, 8)->nullable();
      $table->string('he_ident')->nullable();
      $table->double('he_latitude_deg', 15, 8)->nullable();
      $table->double('he_longitude_deg', 15, 8)->nullable();
      $table->double('he_elevation_ft', 15, 8)->nullable();
      $table->double('he_heading_degT', 15, 8)->nullable();
      $table->double('he_displaced_threshold_ft', 15, 8)->nullable();
      $table->string('airport_ident')->nullable();
      $table->integer('airport_ref')->nullable();

      //foraneas
      // $table->integer('id_airport')->unsigned()->index();
      $table->integer('id_header')->unsigned()->index()->nullable();
      // $table->foreignId('id_airport')->references('ref')->on('airport')->onDelete('cascade');

      // $table->integer('airport_ident')->unsigned()->index()->nullable();

      // $table->foreign('id_airport')->references('ref')->on('airport')->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('id_header')->references('id')->on('header');
      // $table->foreign('id_airport')->references('id')->on('airport');
      // $table->foreign('airport_ident')->references('ident')->on('airport');
    });
  }

  public function down()
  {
    Schema::dropIfExists('airport_rwy');
  }
}
