<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContinentTable extends Migration
{
  public function up()
  {
    Schema::create('continent', function (Blueprint $table) {

      $table->increments('id');
      $table->string('code', 56);
      $table->string('name', 64);
    });
  }

  public function down()
  {
    Schema::dropIfExists('continent');
  }
}
