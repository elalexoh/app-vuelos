<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftDetailTable extends Migration
{
  public function up()
  {
    Schema::create('aircraft_detail', function (Blueprint $table) {

      $table->increments('id');
      $table->string('picture_main');
      $table->string('picture_secundary_1');
      $table->string('picture_secundary_2');
      $table->integer('aircraft_priority');
      $table->string('owner_name');
      $table->string('operator_name');
      $table->string('tail_number');
      $table->double('aircraft_yom');
      $table->text('user_notes');
      $table->text('admin_notes');
      $table->datetime('last_update');
      $table->double('calification');
        // foreaneas
      $table->integer('id_prohibited_airports')->unsigned()->index()->nullable();
        // creando relacion
      $table->foreign('id_prohibited_airports')->references('id')->on('airport');
    });
  }

  public function down()
  {
    Schema::dropIfExists('aircraft_detail');
  }
}
