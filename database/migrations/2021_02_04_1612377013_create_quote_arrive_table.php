<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuoteArriveTable extends Migration
{
  public function up()
  {
    Schema::create('quote_arrive', function (Blueprint $table) {


      $table->increments('id');
      $table->integer('arrive_date');
      $table->integer('id_quote')->unsigned()->index();
      $table->integer('id_region')->unsigned()->index();
      $table->foreign('id_region')->references('id')->on('region');
      $table->foreign('id_quote')->references('id')->on('quote');
      // $table->primary(['id_quote', 'id_region']);
    });
  }

  public function down()
  {
    Schema::dropIfExists('quote_arrive');
  }
}
