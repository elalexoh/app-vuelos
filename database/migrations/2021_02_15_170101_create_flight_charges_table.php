<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightChargesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('flight_charges', function (Blueprint $table) {
      $table->id('id');
      $table->integer('quoted_operator_price')->nullable(); //solo llenar este, preguntar por el criterio elegido
      $table->integer('charger_by_operator')->nullable();
      $table->integer('actual_operator_price')->nullable();
      $table->integer('quoted_to_client')->nullable();
      $table->integer('charged_to_client')->nullable();
      $table->integer('quoted_taxes')->nullable();
      $table->integer('actual_taxes')->nullable();
      $table->integer('quoted_airport_taxes')->nullable();
      $table->integer('actual_airport_taxes')->nullable();
      $table->integer('paid_by_client')->nullable();
      $table->integer('paid_to_operator')->nullable();
      $table->integer('id_flight')->unsigned()->index();

      // relaciones
      $table->foreign('id_flight')->references('id')->on('flights');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('flight_charges');
  }
}
