<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportTable extends Migration
{
  public function up()
  {
    Schema::create('airport', function (Blueprint $table) {

      $table->increments('id');
      $table->integer('ref')->unique();
      $table->float('layover_1')->nullable();
      $table->float('layover_2')->nullable();
      $table->float('layover_3')->nullable();
      $table->float('departure_taxes')->nullable();
      $table->float('arrival_taxes')->nullable();
      $table->float('handling_services')->nullable();
      $table->float('total_taxi_time')->nullable();
      $table->float('turn_around_time')->nullable();
      $table->string('notes')->nullable();
      $table->string('ident')->nullable();
      $table->string('type')->nullable();
      $table->string('name')->nullable();
      $table->string('latitude_deg')->nullable();
      $table->string('longitude_deg')->nullable();
      $table->string('elevation_ft')->nullable();
      $table->string('continent')->nullable();
      $table->string('iso_country')->nullable();
      $table->string('iso_region')->nullable();
      $table->string('municipality')->nullable();
      $table->string('scheduled_service')->nullable();
      $table->string('gps_code')->nullable();
      $table->string('iata_code')->nullable();
      $table->string('local_code')->nullable();
      $table->string('home_link')->nullable();
      $table->string('wikipedia_link')->nullable();
      $table->string('keywords', 550)->nullable();
      $table->float('category')->unsigned()->index()->nullable();
      //foreanea
      $table->integer('id_region')->unsigned()->index()->nullable();
      //referencia
      $table->foreign('id_region')->references('id')->on('region');
    });
  }

  public function down()
  {
    Schema::dropIfExists('airport');
  }
}
