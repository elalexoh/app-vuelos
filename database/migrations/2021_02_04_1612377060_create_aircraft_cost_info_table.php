<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftCostInfoTable extends Migration
{
  public function up()
  {
    Schema::create('aircraft_cost_info', function (Blueprint $table) {

      $table->increments('id');
      $table->boolean('has_segmented_prices'); //si es 1 se usan valores 1,2,3,4,5, si es 0, solo se usará valor 5 y speed 5.
      $table->boolean('has_distance_prices'); //Para ver si el calculo se hace por distancia o por tiempo de vuelo
      $table->boolean('unit_used_km'); //para ver si unidad son KM o NM
      $table->double('min_distance'); //Distancia mínima que se multiplicará al costo, si es > min_dist entonces se multiplica la distancia total
      $table->float('price_1'); //Precio a usar si distancia promedio < distance threshold 1
      $table->float('price_2'); //Precio a usar si distancia promedio < distance threshold 2
      $table->float('price_3'); //Precio a usar si distancia promedio < distance threshold 3
      $table->float('price_4'); //Precio a usar si distancia promedio < distance threshold 4
      $table->float('price_5'); //Precio a usar si distancia promedio > distance threshold 4
      $table->float('distance_th_1'); //Distancia promedio se calculará con estos valores en vez de os fijos del sistema. Tendrán valor por según el que define admin LVL 1 y 2
      $table->float('distance_th_2'); //Distancia promedio se calculará con estos valores en vez de os fijos del sistema. Tendrán valor por según el que define admin LVL 1 y 3
      $table->float('distance_th_3'); //Distancia promedio se calculará con estos valores en vez de os fijos del sistema. Tendrán valor por según el que define admin LVL 1 y 4
      $table->float('distance_th_4'); //Distancia promedio se calculará con estos valores en vez de os fijos del sistema. Tendrán valor por según el que define admin LVL 1 y 5
      $table->float('distance_th_5'); //Distancia promedio se calculará con estos valores en vez de os fijos del sistema. Tendrán valor por según el que define admin LVL 1 y 5
      $table->float('medical_cost_1');
      $table->float('medical_cost_2');
      $table->float('medical_cost_th');
      $table->float('min_trip_lenght');
      $table->float('min_trip_price');
      $table->float('national_taxes');
      $table->float('international_taxes');
      $table->float('national_layovers_cost');
      $table->float('international_layovers_cost');
      $table->float('cabincrew_cost');
      $table->float('avg_speed_1'); //Velocidad a usar si distancia promedio < distance threshold 1
      $table->float('avg_speed_2');
      $table->float('avg_speed_3');
      $table->float('avg_speed_4');
      $table->float('avg_speed_5');
      $table->float('tv_max');
      $table->float('psv_max');
    });
  }

  public function down()
  {
    Schema::dropIfExists('aircraft_cost_info');
  }
}
