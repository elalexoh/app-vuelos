<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHiredFlightsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('hired_flights', function (Blueprint $table) {
      $table->increments('id');
      $table->dateTime('date');
      $table->integer('id_client')->unsigned()->index();
      $table->integer('id_seller')->unsigned()->index();
      $table->integer('id_flight')->unsigned()->index();
      $table->integer('id_book')->unsigned()->index();

      // Relaciones
      $table->foreign('id_client')->references('id')->on('users');
      $table->foreign('id_seller')->references('id')->on('users');
      $table->foreign('id_flight')->references('id')->on('flights');
      $table->foreign('id_book')->references('id')->on('quote');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('hired_flights');
  }
}
