<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightDetailsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('flight_details', function (Blueprint $table) {
      $table->increments('id');

      //$table->boolean('passengers_requests');
      $table->boolean('flight_owner'); // 1 operador || 2 bha || 3 otro
      $table->boolean('allow_contacts_payments')->default(true);
      $table->boolean('allow_contacts_editions')->default(true);
      $table->string('share_link');
      $table->integer('patients'); //1 por cada niño 2 por cada adulto

      // foraneas
      $table->integer('id_calification')->unsigned()->index()->nullable(); //puntuation by user
      $table->integer('id_quoted_aircraft')->unsigned()->index()->nullable(); //aircraft
      // $table->integer('id_requested_legs')->unsigned()->index()->nullable(); //legs
      $table->integer('id_seller')->unsigned()->index()->nullable(); //vendedor de reserva
      $table->integer('id_booking_pax')->unsigned()->index()->nullable(); //pasajero dueño de reserva
      $table->integer('id_flight')->unsigned()->index()->nullable();

      // relaciones
      $table->foreign('id_flight')->references('id')->on('flights');
      $table->foreign('id_calification')->references('id')->on('flight_puntuations');
      $table->foreign('id_quoted_aircraft')->references('id')->on('aircraft');
      // $table->foreign('id_requested_legs')->references('id')->on('requested_legs');
      $table->foreign('id_seller')->references('id')->on('users');
      $table->foreign('id_booking_pax')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('flight_details');
  }
}
