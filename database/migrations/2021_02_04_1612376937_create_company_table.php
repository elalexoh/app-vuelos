<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
  public function up()
  {
    Schema::create('company', function (Blueprint $table) {

      $table->increments('id');
      $table->string('name');
      $table->string('position');
      $table->string('website');
      $table->integer('id_country')->unsigned()->index();
      $table->foreign('id_country')->references('id')->on('country');
    });
  }

  public function down()
  {
    Schema::dropIfExists('company');
  }
}
