<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightPuntuationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('flight_puntuations', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('score')->nullable();
      $table->integer('id_flight')->unsigned()->index();

      // relaciones
      $table->foreign('id_flight')->references('id')->on('flights');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('flight_puntuations');
  }
}
