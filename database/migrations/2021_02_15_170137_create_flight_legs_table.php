<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightLegsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('flight_legs', function (Blueprint $table) {
      $table->increments('id');
      $table->date('leg_date');
      $table->string('rtd');
      $table->string('etd');
      $table->string('atd');
      $table->string('eta');
      $table->string('ata');
      $table->integer('empty_seat_price');
      $table->integer('airport_terminal_indications');
      $table->string('show_up_time');
      $table->integer('leg_capacity');

      // foraneas
      $table->integer('id_arrival_airport')->unsigned()->index()->nullable();
      $table->integer('id_departure_airport')->unsigned()->index()->nullable();
      // $table->integer('id_flight_reports')->unsigned()->index()->nullable();
      // $table->integer('id_crew_members_list')->unsigned()->index()->nullable();
      // $table->integer('id_passengers_list')->unsigned()->index()->nullable();
      $table->integer('id_flight')->unsigned()->index()->nullable();
      $table->integer('id_leg_type')->unsigned()->index()->nullable();

      // relaciones
      $table->foreign('id_arrival_airport')->references('id')->on('airport');
      $table->foreign('id_departure_airport')->references('id')->on('airport');
      // $table->foreign('id_flight_reports')->references('id')->on('airport'); //nose que tabla
      // $table->foreign('id_crew_members_list')->references('id')->on('airport');
      // $table->foreign('id_passengers_list')->references('id')->on('airport');
      $table->foreign('id_flight')->references('id')->on('flights');
      $table->foreign('id_leg_type')->references('id')->on('leg_types');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('flight_legs');
  }
}
