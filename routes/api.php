<?php

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => 'v1'], function () { //v1


  Route::prefix('auth')->group(function () { //Auth
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@register');

    Route::post('send-email-token', 'AppMobile\AppUserController@sendEmailToken');
    Route::post('reset-password', 'AppMobile\AppUserController@changePassReset');


    Route::middleware('auth.jwt')->group(function () {
      Route::post('logout', 'AuthController@logout');
      Route::post('refresh', 'AuthController@refresh');
      Route::post('me', 'AuthController@me');
    });
  });
  Route::middleware('auth.jwt')->group(function () { //Logged
    // reset password inside app
    Route::post('update-password', 'AppMobile\AppUserController@changePassApp');

    //! APP
    Route::get('app-users/{id}', 'AppMobile\AppUserController@user'); //*get user by id
    Route::post('app-users', 'AppMobile\AppUserController@store'); //*save new user
    Route::post('app-users/{id}/profile-info', 'AppMobile\AppUserController@storeDataUser'); //* save profile info
    Route::put('app-users/{id}/profile-info', 'AppMobile\AppUserController@updateDataUser'); //* update profile info
    Route::get('app-airports', 'AirportController@index'); //* AIRPORTS LIST 

    // Users
    Route::get('users', 'UserController@index');
    Route::get('users/{id}', 'UserController@user');
    Route::put('users/{id}', 'UserController@update');
    Route::post('users', 'UserController@store');
    Route::delete('users/{id}', 'UserController@destroy');
    Route::get('users/{id}/my-profile', 'UserController@getMyProfile');
    Route::put('users/{id}/my-profile', 'UserController@updateMyProfile');

    //CrewMembers
    Route::get('crewmembers', 'CrewMembersController@index');
    Route::get('crewmembers/{id}/edit', 'CrewMembersController@edit');
    Route::delete('crewmembers/{id}', 'CrewMembersController@destroy');

    //Airports
    Route::get('airports', 'AirportController@index'); //* AIRPORTS LIST
    Route::delete('airports/{id}', 'AirportController@destroy');
    Route::post('airports', 'AirportController@store');
    Route::put('airports/{id}', 'AirportController@update');
    Route::get('airports/{id}/edit', 'AirportController@edit');



    // Levels
    Route::get('levels', 'LevelController@index');
    Route::get('levels/{id}/edit', 'LevelController@edit');
    Route::put('levels/{id}', 'LevelController@update');
    Route::post('levels', 'LevelController@store');
    Route::delete('levels/{id}', 'LevelController@destroy');

    // Aircrafts
    Route::get('aircrafts', 'AircraftController@index');
    Route::get('aircrafts/{id}', 'AircraftController@aircraft');
    Route::put('aircrafts/{id}', 'AircraftController@update');
    Route::post('aircrafts', 'AircraftController@store');
    Route::delete('aircrafts/{id}', 'AircraftController@destroy');

    // Extras
    Route::get('countries', 'LocationController@indexCountries');
    Route::get('continents', 'LocationController@indexContinents');
    Route::get('regions', 'LocationController@indexRegions');
    Route::get('continents/{id}/contries', 'LocationController@countriesByContinents');
    Route::get('contries/{id}/regions', 'LocationController@regionsbyCountries');
    Route::get('quote', 'QuoteController@GetQuoteByDate');

    // cotizador
    Route::group(['prefix' => 'quote'], function () {
      // Route::get('flights', 'QuoteController@getFlights'); //* Flights
      // Route::get('flights/{id}/request', 'QuoteController@flightRequest'); //* FLights more info 
      // Route::put('flights/{id}/confirm', 'QuoteController@flightConfirm'); //* Aceptar o rechazar vuelo

      // //! flights/date/{date}
      // Route::get('date/flights', 'QuoteController@getFlightsByDate'); //* Flights by date

      // Route::get('airports/{id}/aircrafts', 'QuoteController@getAircraftsByAirport'); //* Aircrafts
      // Route::post('book-flight', 'QuoteController@bookFlight'); //* Reservar vuelo

      Route::get('airports/{query}', 'QuoteController@getAirports'); //* get airport by name or region
      Route::post('aircrafts', 'QuoteController@getAircrafts'); //* get airport by name or region
      Route::put('flights/{id}', 'QuoteController@createReserva'); //* Flight reservation
    });
  });
  Route::group(['prefix' => 'quote'], function () { //Cotizador not logged
  });
});
