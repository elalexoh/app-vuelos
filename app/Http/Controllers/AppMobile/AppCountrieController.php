<?php

namespace App\Http\Controllers\AppMobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;


class AppCountryController extends Controller
{
  public function index()
  {
    $countries = Country::all();
    if (count($countries)) {
      $response = response()->json($countries, 200);
    } else {
      $response = response()->json([
        'status'     => 'Resources not found',
        'message'   => 'No se han encontrado registros'
      ], 404);
    }
    return $response;
  }
}
