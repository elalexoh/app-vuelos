<?php

namespace App\Http\Controllers\AppMobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Passport;
use App\Models\Personal_Information;
use App\Models\Country;
use App\Models\Company;
use App\Models\Reset_Pass;
use DateTime;
use DateInterval;
use Mail;
use App\Mail\ResetPasswordReceived;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class AppUserController extends Controller
{

  public function store(Request $request)
  {

    $params_array = [
      'email'             =>  $request->input('email'),
      'password'          =>  $request->input('password'),
      'confirm_email'     =>  $request->input('confirm_email'),
      'confirm_password'  =>  $request->input('confirm_password'),
    ];
    $params = (object) $params_array;
    if (!empty($params) && !empty($params_array)) {

      //Validamos los datos
      $validate = Validator::make($params_array, [
        'email'             =>  'required|email',
        'password'          =>  'required',
        'confirm_email'     =>  'required',
        'confirm_password'  =>  'required'
      ]);

      if (!$validate->fails()) {
        if (($params->email == $params->confirm_email) && ($params->password == $params->confirm_password)) {
          $today = new DateTime();
          $pwd = Hash::make($params->password);

          $user               = new User;
          $user->username     = $params->email;
          $user->password     = $pwd;
          $user->created_date = $today;
          $user->id_level     = 7;
          $user->save();

          $response = response()->json(array(
            'status'    => 'Success',
            'message'   => 'User created correctly'
          ), 201);
        } else {
          $response = response()->json(array(
            'status'    => 'Error',
            'message'   => 'Los datos no coinciden, verifique nuevamente'
          ), 400);
        }
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validation error',
          'message'   => 'Ha ocurrido un problema',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else { // Si los datos están vacíos
      $response = response()->json(array(
        'status'    => 'No Content',
        'message'   => 'Los datos enviados no son correctos'
      ), 204);
    }

    return $response;
  }

  public function storeDataUser(Request $request, $id)
  {
    $params_array = [
      'first_name'        => $request->input('first_name'),
      'last_name'         => $request->input('last_name'),
      'date_of_birth'     => $request->input('date_of_birth'),
      'gender'            => $request->input('gender'),
      'number_passport'   => $request->input('number_passport'),
      'country_passport'  => $request->input('country_passport'),
      'due_passport'      => $request->input('due_passport'),
      'personal_email'    => $request->input('personal_email'),
      'personal_phone'    => $request->input('personal_phone'),
      'language'          => $request->input('language'),
    ];
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      //Validamos los datos
      $validate = Validator::make($params_array, [
        'first_name'        =>  'required',
        'last_name'         =>  'required',
        'date_of_birth'     =>  'required',
        'gender'            =>  'required',
        'number_passport'   =>  'required',
        'country_passport'  =>  'required',
        'due_passport'      =>  'required',
        'personal_email'    =>  'required',
        'personal_phone'    =>  'required',
        'language'          =>  'required'
      ]);

      if (!$validate->fails()) {

        $user = User::find($id);

        if (is_object($user)) {
          $passport = new Passport;
          $passport->number           = $params->number_passport;
          $passport->due_date         = $params->due_passport;
          $passport->id_country       = $params->country_passport;
          $passport->save();


          $personal_info = new Personal_Information;
          $personal_info->first_name      = $params->first_name;
          $personal_info->last_name       = $params->last_name;
          $personal_info->date_of_birth   = $params->date_of_birth;
          $personal_info->gender          = $params->gender;
          $personal_info->email           = $params->personal_email;
          $personal_info->phone           = $params->personal_phone;
          $personal_info->language        = $params->language;
          $personal_info->id_passport     = $passport->id;
          $personal_info->save();

          $user->id_personal_information = $personal_info->id;
          $user->update();


          $response = response()->json(array(
            'status'    => 'Success',
            'message'   => 'Data User added correctly'
          ), 201);
        } else {
          $response = response()->json(array(
            'status'    => 'Resources not found',
            'message'   =>  'User not found'
          ), 204);
        }
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validations error',
          'message'   => 'Ha ocurrido un problema',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else { // Si los datos están vacíos
      $response = response()->json(array(
        'status'    => 'Empty data',
        'message'   => 'Los datos enviados no son correctos'
      ), 400);
    }

    return $response;
  }

  public function updateDataUser(Request $request, $id)
  {
    $params_array = [
      'first_name'        => $request->input('first_name'),
      'last_name'         => $request->input('last_name'),
      'date_of_birth'     => $request->input('date_of_birth'),
      'gender'            => $request->input('gender'),
      'number_passport'   => $request->input('number_passport'),
      'country_passport'  => $request->input('country_passport'),
      'due_passport'      => $request->input('due_passport'),
      'personal_email'    => $request->input('personal_email'),
      'personal_phone'    => $request->input('personal_phone'),
      'language'          => $request->input('language'),
    ];
    $params = (object) $params_array;
    if (!empty($params) && !empty($params_array)) {

      //Validamos los datos
      $validate = \Validator::make($params_array, [
        'first_name'        =>  'required',
        'last_name'         =>  'required',
        'date_of_birth'     =>  'required',
        'gender'            =>  'required',
        'number_passport'   =>  'required',
        'country_passport'  =>  'required',
        'due_passport'      =>  'required',
        'personal_email'    =>  'required',
        'personal_phone'    =>  'required',
        'language'          =>  'required'
      ]);

      if (!$validate->fails()) {
        $user = User::find($id);

        if (is_object($user)) {

          $personal_info = Personal_Information::find($user->id_personal_information);
          $personal_info->first_name      = $params->first_name;
          $personal_info->last_name       = $params->last_name;
          $personal_info->date_of_birth   = $params->date_of_birth;
          $personal_info->gender          = $params->gender;
          $personal_info->email           = $params->personal_email;
          $personal_info->phone           = $params->personal_phone;
          $personal_info->language        = $params->language;
          $personal_info->update();

          $passport = Passport::find($personal_info->id_passport);
          $passport->number           = $params->number_passport;
          $passport->due_date         = $params->due_passport;
          $passport->id_country       = $params->country_passport;
          $passport->update();


          $response = response()->json(array(
            'status'    => 'Success',
            'message'   => 'Data User updated correctly'
          ), 200);
        } else {
          $response = response()->json(array(
            'status'    => 'Resources not found',
            'message'   =>  'User not found'
          ), 204);
        }
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validation errors',
          'message'   => 'Ha ocurrido un problema',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else { // Si los datos están vacíos
      $response = response()->json(array(
        'status'    => 'Empty data',
        'message'   => 'Los datos enviados no son correctos'
      ), 400);
    }

    return $response;
  }

  public function user($id)
  {
    $user = User::find($id);
    $personal_info = Personal_Information::find($user->id_personal_information);
    $passport = Passport::find($personal_info->id_passport);

    if (is_object($user) && is_object($personal_info) && is_object($passport)) {
      $data = array(
        'first_name'           => $personal_info->first_name,
        'last_name'            => $personal_info->last_name,
        'date_of_birth'        => $personal_info->date_of_birth,
        'gender'               => $personal_info->gender,
        'number_passport'      => $passport->number_passport,
        'countrie_passport'     => $passport->id_countrie,
        'due_passport'         => $passport->due_date,
        'personal_email'       => $personal_info->personal_email,
        'personal_phone'       => $personal_info->personal_phone,
        'language'             => $personal_info->language
      );
      $response = response()->json($data, 200);
    } else {
      $response = response()->json([
        'status'    => 'Resources not found',
        'message'   => 'Mensaje no encontrado'
      ], 204);
    }
    return $response;
  }

  //! por revisar

  // Enviamos token a email de usuario
  public function sendEmailToken(Request $request)
  {
    $today = new DateTime();
    $params_array = [
      'username'        => $request->input('username'),
    ];
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // Validamos los datos
      $validate = Validator::make($params_array, [
        'username' => 'required|email'
      ]);

      if (!$validate->fails()) {
        $user = User::where('username', $params->username)->first();

        if (is_object($user)) {

          $valid_reset = Reset_Pass::where('id_user', $user->id)->first();

          if (!is_object($valid_reset)) {
            //Crear registro en tabla RESET_PASS con un token
            $reset_pass             = new Reset_Pass();
            $reset_pass->token      = sprintf("%06d", mt_rand(1, 999999));
            $reset_pass->date = $today->format('Y-m-d H:i:s');
            $reset_pass->id_user    = $user->id;
            $reset_pass->save();

            //Enviar email con el token
            Mail::to($params->username)->send(new ResetPasswordReceived($user, $reset_pass));

            $response = response()->json(array(
              'status'    => 'success',
              'message'   => 'Email de restablecimiento de contraseña enviado satisfactoriamente',
              'datos'     => $params
            ), 200);
          } else {
            $response = response()->json(array(
              'status'    => 'error',
              'message'   => 'Ya solicitó un código de recuperación'
            ), 400);
          }
        } else {
          $response = response()->json(array(
            'status'    => 'No content',
            'message'   => 'El usuario que solicito no existe'
          ), 201);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Validation Errors',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'No Content',
        'message'   => 'No se han recibido los datos'
      ), 204);
    }
    return $response;
  }

  // Restablecemos contraseña
  public function changePassReset(Request $request)
  {
    $params_array = [
      'newPass'        => $request->input('newPass'),
      'confirmPass'        => $request->input('confirmPass'),
      'token_reset'        => $request->input('token_reset'),
    ];
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {
      $validate = Validator::make($params_array, [
        'newPass'       => 'required',
        'confirmPass'   => 'required',
        'token_reset'   => 'required'
      ]);

      if (!$validate->fails()) {
        // Los datos fueron validados correctamente
        $reset_pass = Reset_Pass::where('token', $params->token_reset)->first();

        if (is_object($reset_pass)) {

          if ($params->newPass == $params->confirmPass) {
            $user = User::find($reset_pass->id_user);
            if (is_object($user)) {
              $pwd = Hash::make($params->newPass);

              $user->password = $pwd;
              $user->update();

              $reset_pass->delete();
              $response = response()->json(
                array(
                  'status'    => 'Success',
                  'message'   => 'Clave de usuario actualizada satisfactoriamente'
                ),
                200
              );
            } else {
              $response = response()->json(array(
                'status'    => 'No Content',
                'message'   => 'No puede realizar esta acción, parece que su usuario ya no existe'
              ), 204);
            }
          } else {

            $response = response()->json(array(
              'status'    => 'Error',
              'message'   => 'Las contraseñas deben ser iguales'
            ), 400);
          }
        } else {

          $response = response()->json(array(
            'status'    => 'Error',
            'message'   => 'No se ha podido validar su identidad'
          ), 400);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Validation errors',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'No Content',
        'message'   => 'No se han recibido los datos',
      ), 400);
    }

    return $response;
  }

  /**Metodo hecho por angel para cambiar password desde la api */
  public function changePassApp(Request $request)
  {
    $user_loged = JWTAuth::user();
    $params_array = [
      'oldPass'            => $request->input('oldPass'),
      'newPass'            => $request->input('newPass'),
      'confirmPass'        => $request->input('confirmPass'),
      'userName'           => $request->input('userName'),
    ];
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {
      // Validamos los datos
      $validate = Validator::make($params_array, [
        'oldPass'       => 'required',
        'newPass'       => 'required',
        'confirmPass'   => 'required',
        'userName'      => 'required'
      ]);

      if (!$validate->fails()) {
        //verifico que la new pass y la confirm sean iguales
        if ($params->newPass == $params->confirmPass) {

          $user = User::where('username', $params->userName)->first();

          //verifico el usuario
          if (is_object($user)) {
            $pwd = Hash::make($params->confirmPass);
            if ($user->id === $user_loged->id) {
              $user->password = $pwd;
              $user->update();
              $response = response()->json(
                [
                  'status'    => 'Success',
                  'message'   => 'Clave de usuario actualizada satisfactoriamente'
                ],
                200
              );
            } else {
              $response = response()->json(
                [
                  'status'    => 'Forbidden',
                  'message'   => 'No tiene permisos para realizar esta accion'
                ],
                403
              );
            }
          } else {

            $response = response()->json(array(
              'status'    => 'No content',
              'message'   => 'No se ha encontrado el usuario que busca'
            ), 204);
          }
        } else {
          $response = response()->json(array(
            'status'    => 'Validations errors',
            'message'   => 'Las contraseñas no son iguales'
          ), 400);
        }
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validations errors',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Empty request',
        'message'   => 'No se han recibido los datos',
        'datos'     => $params
      ), 400);
    }

    return $response;
  }
}
