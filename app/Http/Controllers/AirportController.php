<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Airport;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\AirportImport;
use App\Models\Aircraft;
use App\Models\User;
use App\Models\Aircraft_Model;
use App\Models\Type_Operation;

class AirportController extends Controller
{
  public function index()
  {
    $airports_data = Airport::all();
    $airports = [];

    if (count($airports_data)) {
      foreach ($airports_data as $airport) {
        $airports[] = [
          'id'            => $airport->id,
          'name'          => $airport->name,
          'iso_country'   => $airport->iso_country,
          'iso_region'    => $airport->iso_region
        ];
      }
      $response = response()->json($airports, 200);
    } else {
      $response = response()->json(array(
        'status'    =>  'Resources not found',
        'message'   =>  'No se han encontrado registros'
      ), 404);
    }
    return $response;
  }

  public function store(Request $request)
  {

    $params_array = array(
      "ident"               =>  $request->input("ident"),
      "type"                =>  $request->input("type"),
      "name"                =>  $request->input("name"),
      "latitude_deg"        =>  $request->input("latitude_deg"),
      "longitude_deg"       =>  $request->input("longitude_deg"),
      "elevation_ft"        =>  $request->input("elevation_ft"),
      "continent"           =>  $request->input("continent"),
      "iso_country"         =>  $request->input("iso_country"),
      "iso_region"          =>  $request->input("iso_region"),
      "municipality"        =>  $request->input("municipality"),
      "scheduled_service"   =>  $request->input("scheduled_service"),
      "gps_code"            =>  $request->input("gps_code"),
      "iata_code"           =>  $request->input("iata_code"),
      "local_code"          =>  $request->input("local_code"),
      "home_link"           =>  $request->input("home_link"),
      "wikipedia_link"      =>  $request->input("wikipedia_link"),
      "keywords"            =>  $request->input("keywords"),
    );

    // return $params_array;
    $params = (object) $params_array;
    if (!empty($params) && !empty($params_array)) {
      //Validamos los datos
      $validate = \Validator::make($params_array, [
        'ident'                => 'required|alpha_num',
        'type'                 => 'required|integer',
        'name'                 => 'required',
        // 'latitude_deg'         => 'required',
        // 'longitude_deg'        => 'required',
        // 'elevation_ft'         => 'required',
        // 'continent'            => 'required',
        // 'iso_country'          => 'required',
        // 'iso_region'           => 'required',
        // 'municipality'         => 'required',
        // 'scheduled_service'    => 'required',
        // 'gps_code'             => 'required',
        // 'iata_code'            => 'required',
        // 'local_code'           => 'required',
        // 'home_link'            => 'required',
        // 'wikipedia_link'       => 'required',
        // 'keywords'             => 'required',
      ]);
      if (!$validate->fails()) {

        $airport                        = new Airport;
        $airport->ident                 = $params->ident;
        $airport->type                  = $params->type;
        $airport->name                  = $params->name;
        $airport->latitude_deg          = $params->latitude_deg;
        $airport->longitude_deg         = $params->longitude_deg;
        $airport->elevation_ft          = $params->elevation_ft;
        $airport->continent             = $params->continent;
        $airport->iso_country           = $params->iso_country;
        $airport->iso_region            = $params->iso_region;
        $airport->municipality          = $params->municipality;
        $airport->scheduled_service     = $params->scheduled_service;
        $airport->gps_code              = $params->gps_code;
        $airport->iata_code             = $params->iata_code;
        $airport->local_code            = $params->local_code;
        $airport->home_link             = $params->home_link;
        $airport->wikipedia_link        = $params->wikipedia_link;
        $airport->keywords              = $params->keywords;

        $airport->save();


        $response = response()->json(array(
          'status'    => 'Success',
          'message'    => 'Airport registered sucessfully',
        ), 200);
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validation Errors',
          'message'   => 'There was a problem with the validation data',
          'errors'    => $validate->errors(),
        ), 404);
      }
    } else {
      // Request vacio
      $response = response()->json(array(
        'status'    => 'No Content',
        'message'   => 'Data has not been received',
      ), 404);
    }
    return $response;
  }

  public function edit($id)
  {
    $airport = Airport::find($id);

    if (is_object($airport)) {

      $response = response()->json($airport, 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Resources not found',
        'message'   => 'Airport not found',
      ), 404);
    }
    return $response;
  }

  public function update(Request $request, $id)
  {

    $params_array = array(
      "ident"               =>  $request->input("ident"),
      "type"                =>  $request->input("type"),
      "name"                =>  $request->input("name"),
      "latitude_deg"        =>  $request->input("latitude_deg"),
      "longitude_deg"       =>  $request->input("longitude_deg"),
      "elevation_ft"        =>  $request->input("elevation_ft"),
      "continent"           =>  $request->input("continent"),
      "iso_country"         =>  $request->input("iso_country"),
      "iso_region"          =>  $request->input("iso_region"),
      "municipality"        =>  $request->input("municipality"),
      "scheduled_service"   =>  $request->input("scheduled_service"),
      "gps_code"            =>  $request->input("gps_code"),
      "iata_code"           =>  $request->input("iata_code"),
      "local_code"          =>  $request->input("local_code"),
      "home_link"           =>  $request->input("home_link"),
      "wikipedia_link"      =>  $request->input("wikipedia_link"),
      "keywords"            =>  $request->input("keywords"),
    );

    // return $params_array;
    $params = (object) $params_array;
    if (!empty($params) && !empty($params_array)) {
      //Validamos los datos
      $validate = \Validator::make($params_array, [
        'ident'                => 'required|alpha_num',
        // 'type'                 => 'required|integer',
        'name'                 => 'required',
        // 'latitude_deg'         => 'required',
        // 'longitude_deg'        => 'required',
        // 'elevation_ft'         => 'required',
        // 'continent'            => 'required',
        // 'iso_country'          => 'required',
        // 'iso_region'           => 'required',
        // 'municipality'         => 'required',
        // 'scheduled_service'    => 'required',
        // 'gps_code'             => 'required',
        // 'iata_code'            => 'required',
        // 'local_code'           => 'required',
        // 'home_link'            => 'required',
        // 'wikipedia_link'       => 'required',
        // 'keywords'             => 'required',
      ]);
      if (!$validate->fails()) {

        $airport                        = Airport::find($id);
        if (is_object($airport)) {
          $airport->ident              = is_null($request->input('ident')) ? $airport->ident : $request->input('ident');
          $airport->type               = is_null($request->input('type')) ? $airport->type : $request->input('type');
          $airport->name               = is_null($request->input('name')) ? $airport->name : $request->input('name');
          $airport->latitude_deg       = is_null($request->input('latitude_deg')) ? $airport->latitude_deg : $request->input('latitude_deg');
          $airport->longitude_deg      = is_null($request->input('longitude_deg')) ? $airport->longitude_deg : $request->input('longitude_deg');
          $airport->elevation_ft       = is_null($request->input('elevation_ft')) ? $airport->elevation_ft : $request->input('elevation_ft');
          $airport->continent          = is_null($request->input('continent')) ? $airport->continent : $request->input('continent');
          $airport->iso_country        = is_null($request->input('iso_country')) ? $airport->iso_country : $request->input('iso_country');
          $airport->iso_region         = is_null($request->input('iso_region')) ? $airport->iso_region : $request->input('iso_region');
          $airport->municipality       = is_null($request->input('municipality')) ? $airport->municipality : $request->input('municipality');
          $airport->scheduled_service  = is_null($request->input('scheduled_service')) ? $airport->scheduled_service : $request->input('scheduled_service');
          $airport->gps_code           = is_null($request->input('gps_code')) ? $airport->gps_code : $request->input('gps_code');
          $airport->iata_code          = is_null($request->input('iata_code')) ? $airport->iata_code : $request->input('iata_code');
          $airport->local_code         = is_null($request->input('local_code')) ? $airport->local_code : $request->input('local_code');
          $airport->home_link          = is_null($request->input('home_link')) ? $airport->home_link : $request->input('home_link');
          $airport->wikipedia_link     = is_null($request->input('wikipedia_link')) ? $airport->wikipedia_link : $request->input('wikipedia_link');
          $airport->keywords           = is_null($request->input('keywords')) ? $airport->keywords : $request->input('keywords');

          $airport->update();

          $data = response()->json(array(
            'status'    => 'Success',
            'message'    => 'Airport update sucessfully',
          ), 200);
        } else {
          $data = response()->json(array(
            'status'    => 'Resources not found',
            'message'   => 'Airport not found',
          ), 404);
        }
      } else {
        $data = response()->json(array(
          'status'    => 'Validation Errors',
          'message'   => 'there was a problem with the validation data',
          'errors'    => $validate->errors(),
        ), 404);
      }
    } else {
      // Request vacio
      $data = response()->json(array(
        'status'    => 'No Content',
        'message'   => 'Data has not been received',
      ), 404);
    }
    return $data;
  }

  public function destroy(Request $request, $id)
  {

    $airport = Airport::find($id);

    if (is_object($airport)) {
      $airport->delete();

      $response = response()->json(array(
        'status'    =>  'Sucess',
        'message'   =>  'Airpot with id: ' . $airport->id . ' and name: ' . $airport->name . ' was sucessfully delete',
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    =>  'Resources not found',
        'message'   =>  'Airport not found',
      ), 404);
    }
    return $response;
  }
}
