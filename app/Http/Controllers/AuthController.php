<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
  public function login(Request $request)
  {
    $params_array = [
      'username'        => $request->input('username'),
      'password'        => $request->input('password'),
    ];
    $validator = Validator::make($params_array, [
      'username' => 'required',
      'password' => 'required',
    ]);
    if ($validator->fails()) {
      return response()->json([
        'status' => 'Validations errors',
        'errors' => $validator->errors()
      ], 422);
    }
    if (!$token = JWTAuth::attempt([
      'username' => $request->username,
      'password' => $request->password
    ])) {
      return response()->json(['status' => 'Unauthorized'], 401);
    }
    return response()->json([
      'status' => 'Success',
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => JWTAuth::factory()->getTTL() * 60
    ]);
  }

  public function logout()
  {
    JWTAuth::invalidate();
    return response()->json([
      'message' => 'Successfully logged out'
    ]);
  }

  public function refresh()
  {
    return response()->json([
      'access_token' => JWTAuth::refresh(),
      'token_type' => 'bearer',
      'expires_in' => JWTAuth::factory()->getTTL() * 60
    ]);
  }

  public function me()
  {
    return response()->json(
      JWTAuth::user()
    );
  }

  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'username' => 'required|email|max:255|unique:users',
      'password' => 'required|string|min:8|max:255|confirmed',
      'password_confirmation' => 'required|string|min:8|max:255',
      'id_level' => 'required|integer|max:1',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'status' => 'error',
        'messages' => $validator->messages()
      ], 200);
    }
    $user = new User();
    $user->username                 = $request->input('username');
    $user->password                 = Hash::make($request->password);
    $user->id_level                 = $request->input('id_level');
    $user->created_date             = date("Y-m-d");
    // $user->id_personal_information 	= ;
    $user->save();

    return response()->json([
      'status' => 'success',
      'data' => $user
    ], 200);
  }
}
