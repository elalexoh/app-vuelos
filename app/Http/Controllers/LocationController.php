<?php

namespace App\Http\Controllers;

use App\Models\Continent;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Http\Request;

class LocationController extends Controller
{



  public function indexCountries()
  {
    $countries = Country::all();
    if (count($countries)) {
      $response = response()->json($countries, 200);
    } else {
      $response = response()->json([
        'status' => 'No Content',
        'message' => 'No se encontraron paises',
      ], 204);
    }
    return $response;
  }

  public function indexContinents()
  {
    $continents = Continent::all();
    if (count($continents)) {
      $response = response()->json($continents, 200);
    } else {
      $response = response()->json([
        'status' => 'No Content',
        'message' => 'No se encontraron continentes',
      ], 204);
    }
    return $response;
  }

  public function indexRegions()
  {
    $regions = Region::all();
    if (count($regions)) {
      $response = response()->json($regions, 200);
    } else {
      $response = response()->json([
        'status' => 'No Content',
        'message' => 'No se encontraron regiones',
      ], 204);
    }
    return $response;
  }

  public function countriesByContinents($id)
  {
    $contries = Country::where('id_continent', $id)->get();
    if (count($contries)) {
      $response = response()->json($contries, 200);
    } else {
      $response = response()->json([
        "status" => "No Content",
        "message" => "No se han encontrado registros",
      ], 204);
    }
    return $response;
  }

  public function regionsbyCountries($id)
  {
    $regions = Region::where('id_country', $id)->get();
    if (count($regions)) {
      $response = response()->json($regions, 200);
    } else {
      $response = response()->json([
        "status" => "No Content",
        "message" => "No se han encontrado registros",
      ], 204);
    }
    return $response;
  }
}
