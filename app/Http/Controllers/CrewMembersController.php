<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Crewmember_Detail;
use App\Models\Personal_Information;
use App\Models\User;

class CrewMembersController extends Controller
{
  public function index()
  {
    $users = User::where('id_crewmember_detail', '!=', null)->get();
    $user_data = [];
    if (count($users)) {
      foreach ($users as $user) {
        $crewmember = Crewmember_Detail::where('id', $user->id_crewmember_detail)->first();
        $personal_info = Personal_Information::where('id', $user->id_personal_information)->first();
        $user_data[] = [
          "personal_info" => $personal_info,
          "license_type"  => $crewmember->license_type,
          "ratings"       => $crewmember->ratings
        ];
      }
      // $crewmembers = Crewmember_Detail::find($user->id_crewmember_detail);
      $response = response()->json($user_data, 200);
    } else {
      $response = response()->json(array(
        'status'    =>  'Resources not found',
        'message'   =>  'No se han encontrado registros'
      ), 204);
    }
    return $response;
  }
  public function edit($id)
  {
    $crewmember = Crewmember_Detail::find($id);
    if (is_object($crewmember)) {
      // edit code
      $response = response()->json($crewmember, 200);
    } else {
      $response = response()->json(array(
        'status'    =>  'Resources not found',
        'message'   =>  'No se han encontrado registros'
      ), 404);
    }
    return $response;
  }
  public function destroy($id)
  {
    $crewmember = Crewmember_Detail::find($id);
    if (is_object($crewmember)) {
      // destroy code
      $response = response()->json($crewmember, 200);
    } else {
      $response = response()->json(array(
        'status'    =>  'Resources not found',
        'message'   =>  'No se han encontrado registros'
      ), 404);
    }
    return $response;
  }
}
