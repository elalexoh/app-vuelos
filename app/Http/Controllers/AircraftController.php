<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aircraft;
use App\Models\User;
use App\Models\Aircraft_Model;
use App\Models\Airport;
use App\Models\Type_Operation;
use Tymon\JWTAuth\Facades\JWTAuth;

class AircraftController extends Controller
{
  public function index()
  {
    $aircrafts = Aircraft::all();
    $aircraf_data = [];

    if (is_object($aircrafts)) {
      foreach ($aircrafts as $aircraft) {
        $user = User::find($aircraft->id_user_owner);
        $aircraft_model = Aircraft_Model::find($aircraft->id_aircraft_model);
        $type = Type_Operation::find($aircraft->id_type_operation);
        $aircraf_data[] = [
          'id'            =>  $aircraft->id,
          'certification' =>  $aircraft->certification,
          'name'          =>  $aircraft_model->name,
          'manufacter'    =>  $aircraft_model->manufacter,
          'capacity'      =>  $aircraft_model->capacity,
          'type'          =>  $type->name,
          'user'          =>  $user->username
        ];

        $response = response()->json($aircraf_data, 200);
      }
    } else {
      $response = response()->json(array(
        'status'    =>  'Resources not found',
        'message'   =>  'No se han encontrado registros'
      ), 204);
    }
    return $response;
  }

  public function store(Request $request)
  {
    $admin_level = JWTAuth::user()->id_level;
    if ($admin_level == 1 || $admin_level == 2 || $admin_level == 3) {
      $params_array = [
        'name'              => $request->input('name'),
        'manufacter'        => $request->input('manufacter'),
        'capacity'          => $request->input('capacity'),
        'certification'     => $request->input('certification'),
        'type'              => $request->input('type'),
        'user'              => $request->input('user')
      ];
      $params = (object) $params_array;

      // Validamos datos
      $validate = \Validator::make($params_array, [
        'name'              => 'required',
        'manufacter'        => 'required',
        'capacity'          => 'required',
        'certification'     => 'required',
        'type'              => 'required',
        'user'              => 'required'
      ]);

      if (!$validate->fails()) {
        $type = new Type_Operation;
        $type->name = $params->type;
        $type->save();

        $aircraft_model             = new Aircraft_Model;
        $aircraft_model->name       = $params->name;
        $aircraft_model->manufacter = $params->manufacter;
        $aircraft_model->capacity   = $params->capacity;
        $aircraft_model->save();

        $aircraft                       = new Aircraft;
        $aircraft->certification        = $params->certification;
        $aircraft->id_user_owner        = $params->user;
        $aircraft->id_aircraft_model    = $aircraft_model->id;
        $aircraft->id_type_operation    = $type->id;
        $aircraft->save();

        // Devolver array con resultado
        $response = response()->json(array(
          'status'    => 'Success',
          'message'   => 'Aircraft values save correctly'
        ), 200);
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validation Error',
          'message'   => 'Check yours validations fields',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Forbidden Access',
        'message'   => 'No posee los permisos necesarios para realizar esta acción'
      ), 403);
    }
    return $response;
  }

  public function aircraft($id)
  {
    $aircraft = Aircraft::find($id);
    $aircraft_model = Aircraft_Model::find($aircraft->id_aircraft_model);
    $type = Type_Operation::find($aircraft->id_type_operation);

    if (is_object($aircraft) && is_object($aircraft_model) && is_object($type)) {
      $data = array(
        'id'            => $aircraft->id,
        'certification' => $aircraft->certification,
        'name'          => $aircraft_model->name,
        'manufacter'    => $aircraft_model->manufacter,
        'capacity'      => $aircraft_model->capacity,
        'type'          => $type->name,
        'user'          => $aircraft->id_user_owner,
      );
      $response = response()->json([
        'status'    => 'Success',
        'aircraft'  => $data
      ], 200);
    } else {
      $response = response()->json([
        'status'    => 'Error',
        'message'  => 'ha ocurrido un error al buscar el registro'
      ], 400);
    }
    return $response;
  }

  public function update(Request $request, $id)
  {
    $admin_level = JWTAuth::user()->id_level;
    if ($admin_level == 1 || $admin_level == 2 || $admin_level == 3) {
      $params_array = [
        'name'              => $request->input('name'),
        'manufacter'        => $request->input('manufacter'),
        'capacity'          => $request->input('capacity'),
        'certification'     => $request->input('certification'),
        'type'              => $request->input('type'),
        'user'              => $request->input('user')
      ];

      // Validamos datos
      $validate = \Validator::make($params_array, [
        'name'              => 'required',
        'manufacter'        => 'required',
        'capacity'          => 'required',
        'certification'     => 'required',
        'type'              => 'required',
        'user'              => 'required'
      ]);

      if (!$validate->fails()) {
        // Actualizar level en la bdd
        $aircraft = Aircraft::find($id);
        $aircraft_model = Aircraft_Model::find($aircraft->id_aircraft_model);
        $type = Type_Operation::find($aircraft->id_type_operation);

        $aircraft->certification = $params_array['certification'];
        $aircraft->id_user_owner = $params_array['user'];
        $aircraft->update();

        $type->name = $params_array['type'];
        $type->update();

        $aircraft_model->name       = $params_array['name'];
        $aircraft_model->manufacter = $params_array['manufacter'];
        $aircraft_model->capacity   = $params_array['capacity'];
        $aircraft_model->update();

        // Devolver array con resultado
        $response = response()->json(array(
          'status'    => 'Success',
          'message'   => 'Aircraft values updated correctly'
        ), 200);
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validation Error',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Forbidden Access',
        'message'   => 'No posee los permisos necesarios para realizar esta acción'
      ), 403);
    }
    return $response;
  }

  public function destroy($id)
  {
    $admin_level = JWTAuth::user()->id_level;
    if ($admin_level == 1 || $admin_level == 2 || $admin_level == 3) {
      $aircraft = Aircraft::find($id);

      if (is_object($aircraft)) {
        $aircraft->delete();

        $response = response()->json(array(
          'status'    => 'Success',
          'message'   => 'Aircraft con id: ' . $aircraft->id . ', eliminado correctamente'
        ), 200);
      } else {
        $response = response()->json(array(
          'status'    => 'Resources not found',
          'message'   => 'Aircraft seleccionado no existe'
        ), 204);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Forbidden Access',
        'message'   => 'No posee los permisos necesarios para realizar esta acción'
      ), 403);
    }
    return $response;
  }
}
