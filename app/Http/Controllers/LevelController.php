<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Level;
use Tymon\JWTAuth\Facades\JWTAuth;

class LevelController extends Controller
{
  public function index()
  {
    $user_level = JWTAuth::user()->id_level;
    if ($user_level === 1) {
      $levels = Level::all();
    } else {
      $levels = Level::where('id', '>', $user_level)->get();
    }
    // $levels = Level::all();
    if (count($levels)) {
      $response = response()->json($levels, 200);
    } else {
      $response = response()->json(array(
        'status' => 'Resources not found',
        'message' => 'No hay levels registrados en la plataforma',
      ), 404);
    }
    return $response;
  }

  public function store(Request $request)
  {
    $json = $request->input('json', null);

    // Recibimos el token en el header.
    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {
      $identity = $jwtAuth->checkToken($token, true);  // Obtenemos los datos del usuario identificado

      if ($identity->id_level == 1 || $identity->id_level == 2 || $identity->id_level == 3) {
        // Se decodifica el json
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {

          //Validamos los datos
          $validate = \Validator::make($params_array, [
            'name'     => 'required'
          ]);

          if ($validate->fails()) {
            // Validaciones fallan
            $response = response()->json(array(
              'status'    => 'Validation Errors',
              'message'   => 'Ha ocurrido un problema',
              'errors'    => $validate->errors()
            ), 400);
          } else {
            $level              = new Level;
            $level->name        = $params->name;
            $level->description = $params->description;
            $level->save();

            $response = response()->json(array(
              'status'    => 'success',
              'message'   => 'Level created correctly'
            ), 200);
          }
        } else { // Si los datos están vacíos
          $response = response()->json(array(
            'status'    => 'No Content',
            'message'   => 'Los datos enviados no son correctos'
          ), 400);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Unauthorized',
          'message'   => 'No posee los permisos necesarios para realizar esta acción'
        ), 401);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Unauthorized',
        'message'   => 'No posee los permisos necesarios para realizar esta acción'
      ), 401);
    }

    return $response;
  }

  public function edit($id)
  {
    $level = Level::find($id);
    if (is_object($level)) {
      $response = response()->json([
        'status' => 'Success',
        'level'   => $level
      ], 200);
    } else {
      $response = response()->json([
        'status' => 'Resources not Found',
        'level'   => $level
      ], 404);
    }
    return $response;
  }

  public function update(Request $request, $id)
  {
    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {
      $identity = $jwtAuth->checkToken($token, true);
      if ($identity->id_level == 1 || $identity->id_level == 2 || $identity->id_level == 3) {

        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        // Validamos datos
        $validate = \Validator::make($params_array, [
          'name' => 'required'
        ]);

        if ($validate->fails()) {
          // Validaciones fallan
          $response = response()->json(array(
            'status'    => 'Validation errors',
            'message'   => 'Ha ocurrido un problema con la validación de los datos',
            'errors'    => $validate->errors()
          ), 400);
        } else {
          // Actualizar level en la bdd
          $level = Level::where('id', $id)->update($params_array);

          $response = response()->json(array(
            'status'    => 'success',
            'message'   => 'Level updated correctly'
          ), 200);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Unauthorized',
          'message'   => 'No posee los permisos necesarios para realizar esta acción'
        ), 401);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Unauthorized',
        'message'   => 'No posee los permisos necesarios para realizar esta acción'
      ), 401);
    }

    return $response;
  }

  public function destroy(Request $request, $id)
  {
    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {
      $identity = $jwtAuth->checkToken($token, true);  // Obtenemos los datos del usuario identificado

      if ($identity->id_level == 1 || $identity->id_level == 2 || $identity->id_level == 3) {
        $level = Level::find($id);

        if (is_object($level)) {
          $level->delete();

          $response = response()->json(array(
            'status'    => 'Success',
            'message'   => 'Level con id: ' . $level->id . ', eliminado correctamente'
          ), 200);
        } else {
          $response = response()->json(array(
            'status'    => 'Resources not found',
            'message'   => 'Level seleccionado no existe'
          ), 404);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Unauthorized',
          'message'   => 'No posee los permisos necesarios para realizar esta acción'
        ), 401);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Unauthorized',
        'message'   => 'Login incorrecto'
      ), 401);
    }

    return $response;
  }
}
