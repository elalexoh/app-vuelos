<?php

namespace App\Http\Controllers;

use App\Models\Personal_Information;
use Illuminate\Http\Request;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{

  //* get admins Nivel (nivel 1 a 5)
  //* get crewmembers (Nivel 6)
  //* get usuarios (nivel 7)
  public function index()
  {
    $user_level = JWTAuth::user()->id_level;
    if ($user_level === 1) {
      $users = User::all();
    } else {
      $users = User::where('id_level', '>', $user_level)->get();
    }

    // $users = User::all();
    $users_data = [];
    if (count($users)) {
      foreach ($users as $user) {
        $personal_info = Personal_Information::where('id', $user->id_personal_information)->first();
        $users_data[] = [
          "id" => $user->id,
          "username" => $user->username,
          "level" => $user->level,
          "personal_info" => $personal_info,

        ];
      }
      $response = response()->json($users_data, 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Resources not found',
        'message'   => 'No se han encontrado registros'
      ), 404);
    }
    return $response;
  }

  public function store(Request $request)
  {
    $admin_level = JWTAuth::user()->id_level;
    if ($admin_level === 1) { //administrador
      $user_request = [
        "username" => $request->input('username'),
        "password" => $request->input('password'),
        "id_level" => $request->input('id_level'),
      ];
      $validate = Validator::make($user_request, [
        'username'     => 'required|email',
        'password'     => 'required',
        'id_level'     => 'required',
      ]);
      if (!$validate->fails()) {
        $user = new User();
        $user->username         = $user_request['username'];
        $user->password         = Hash::make($user_request['password']);
        $user->id_level         = $user_request['id_level'];
        $user->created_date     = date('Y-m-d');
        $user->save();
        $response = response()->json(array(
          'status'    => 'Success',
          'message'   => 'Usuario registrado correctamente',
        ), 200);
      } else {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validation Error',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 400);
      }
    } else if ($admin_level === 2) { //Nivel 2
      if ($request->input('id_level') >= 2) {
        $user_request = [
          "username" => $request->input('username'),
          "password" => $request->input('password'),
          "id_level" => $request->input('id_level'),
        ];
        $validate = Validator::make($user_request, [
          'username'     => 'required|email',
          'password'     => 'required',
          'id_level'     => 'required',
        ]);
        if (!$validate->fails()) {
          $user = new User();
          $user->username         = $user_request['username'];
          $user->password         = Hash::make($user_request['password']);
          $user->id_level         = $user_request['id_level'];
          $user->created_date     = date('Y-m-d');
          $user->save();
          $response = response()->json(array(
            'status'    => 'Success',
            'message'   => 'Usuario registrado correctamente',
          ), 200);
        } else {
          // Validaciones fallan
          $response = response()->json(array(
            'status'    => 'Validation Error',
            'message'   => 'Ha ocurrido un problema con la validación de los datos',
            'errors'    => $validate->errors()
          ), 400);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Forbidden Access',
          'message'   => 'No posee los permisos necesarios para realizar esta acción'
        ), 403);
      }
    } else { //Nivel 3 o mayor
      if ($request->input('id_level') > $admin_level) {
        $user_request = [
          "username" => $request->input('username'),
          "password" => $request->input('password'),
          "id_level" => $request->input('id_level'),
        ];
        $validate = Validator::make($user_request, [
          'username'     => 'required|email',
          'password'     => 'required',
          'id_level'     => 'required',
        ]);
        if (!$validate->fails()) {
          $user = new User();
          $user->username         = $user_request['username'];
          $user->password         = Hash::make($user_request['password']);
          $user->id_level         = $user_request['id_level'];
          $user->created_date     = date('Y-m-d');
          $user->save();
          $response = response()->json(array(
            'status'    => 'Success',
            'message'   => 'Usuario registrado correctamente',
          ), 200);
        } else {
          // Validaciones fallan
          $response = response()->json(array(
            'status'    => 'Validation Error',
            'message'   => 'Ha ocurrido un problema con la validación de los datos',
            'errors'    => $validate->errors()
          ), 400);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Forbidden Access',
          'message'   => 'No posee los permisos necesarios para realizar esta acción'
        ), 403);
      }
    }

    return $response;
  }

  public function user($id)
  {
    $user = User::find($id);
    if (is_object($user)) {
      $response = response()->json([
        "id"       => $user->id,
        "username" => $user->username,
        "id_level" => $user->id_level,
        "password" => null,
      ], 200);
    } else {
      $response = response()->json([
        'status'    => 'Resources not found',
        'message'   => 'No se han encontrado registros'
      ], 204);
    }
    return $response;
  }

  public function update(Request $request, $id)
  {
    $user_request = [
      "username" => $request->input('username'),
      "password" => $request->input('password'),
      "id_level" => $request->input('id_level'),
    ];
    $validate = \Validator::make($user_request, [
      'username'     => 'required|email',
      'password'     => 'required',
      'id_level'     => 'required',
    ]);
    if (!$validate->fails()) {
      $user = User::find($id);
      $user->username         = $user_request['username'];
      $user->password         = Hash::make($user_request['password']);
      $user->id_level         = $user_request['id_level'];
      $user->update();
      $response = response()->json(array(
        'status'    => 'Success',
        'message'   => 'Usuario actualizado correctamente',
      ), 200);
    } else {
      // Validaciones fallan
      $response = response()->json(array(
        'status'    => 'Validation Error',
        'message'   => 'Ha ocurrido un problema con la validación de los datos',
        'errors'    => $validate->errors()
      ), 400);
    }
    return $response;
  }

  //! duda
  public function destroy($id)
  {
    $admin_level = JWTAuth::user()->id_level;
    if ($admin_level == 1 || $admin_level == 2 || $admin_level == 3) {
      $user = User::find($id);

      if (is_object($user)) {
        $user->delete();

        $response = response()->json(array(
          'status'    => 'Success',
          'message'   => 'Usario, eliminado correctamente'
        ), 200);
      } else {
        $response = response()->json(array(
          'status'    => 'Not Content',
          'message'   => 'User seleccionado no existe'
        ), 204);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Forbidden Access',
        'message'   => 'No posee los permisos necesarios para realizar esta acción'
      ), 403);
    }
    return $response;
  }

  public function getMyProfile()
  {
    $user_loged = JWTAuth::user();
    $user = User::find($user_loged->id);
    if (is_object($user)) {
      $response = response()->json([
        // "user_info" => $user,
        "personal_info" => $user->personal_information,
        "level" => $user->level
      ], 200);
    } else {
      $response = response()->json([
        'status'  => 'No content',
        'message' => 'No hemos encontrado su perfil'
      ], 204);
    }
    return $response;
  }

  public function updateMyProfile()
  {
    $user_loged = JWTAuth::user();
    return $user_loged;
  }
}
