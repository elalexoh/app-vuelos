<?php

namespace App\Http\Controllers;

use App\Classes\CustomsCalculated;
use App\Models\Aircraft;
use App\Models\Aircraft_Cost_Info;
use App\Models\Aircraft_Detail;
use App\Models\Aircraft_Detail_Service;
use App\Models\Aircraft_Model;
use App\Models\Aircraft_Service_Level;
use Illuminate\Http\Request;
use App\Models\Airport;
use App\Models\Airport_Rwy;
use App\Models\Country;
use App\Models\Flight_Charge;
use App\Models\Flight_Detail;
use App\Models\Flight_Leg;
use App\Models\Flight_Puntuation;
use App\Models\Flights;
use App\Models\Globals;
use App\Models\Prohibited_Airports;
use App\Models\Region;
use App\Models\Type_Aircraft_Group;
use App\Models\Type_Operation;
use App\Models\User;
use App\Notifications;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\VarDumper\VarDumper;
use Tymon\JWTAuth\Facades\JWTAuth;

class QuoteController extends Controller
{

  public function createReserva($id)
  {

    try {
      $flight = Flights::findOrFail($id);
      $flight->entry_status = 4;
      $flight->update();
      $response = response()->json([
        'status'    => 'Success',
        'message'   => 'EL vuelo fue reservado Exitosamente'
      ], 200);
    } catch (\Throwable $th) {
      $response = response()->json([
        'status'    => 'Not Content',
        'message'   => 'No se ha encontrado ese vuelo'
      ], 204);
    }
    return $response;
  }
  //TODO Falta por evaluar
  public function createFlight(Request $request)
  {
    $params_array = [
      "flight_type"       => $request->input('flight_type'), //one_way, round_trip, multiple || id
      "charter"           => $request->input('charter'), //charter, empty_leg, médico, carga
      "legs"              => $request->input('legs'), //[{arrive_airport, departure_airport, distance, date}]
      "region"            => $request->input('region'), //id
      "airport"           => $request->input('airport'), //id
      "passengers"        => $request->input('passengers'), //id
    ];
    $params = (object) $params_array;

    try {
      DB::beginTransaction();

      $flight = new Flights();
      $flight->calculated_legs                    = "info";
      $flight->flight_date                        = "info";
      $flight->caculated_flight_time              = "info";
      $flight->flight_condition                   = "info";
      $flight->entry_status                       = "info";
      $flight->id_departure_airport               = "info";
      $flight->id_arrival_airport                 = "info";
      $flight->id_aircraft                        = "info";
      $flight->save();

      $flight_detail = new Flight_Detail();
      $flight_detail->flight_owner                = "info";
      $flight_detail->allow_contacts_payments     = "info";
      $flight_detail->allow_contacts_editions     = "info";
      $flight_detail->share_link                  = "info";
      $flight_detail->patients                    = "info";
      $flight_detail->id_calification             = "info";
      $flight_detail->id_quoted_aircraft          = "info";
      $flight_detail->id_requested_legs           = "info";
      $flight_detail->id_seller                   = "info";
      $flight_detail->id_booking_pax              = "info";
      $flight_detail->id_flight                   = $flight->id;
      $flight_detail->save();

      // $flight = new Requested_Leg();

      $flight_leg = new Flight_Leg();
      $flight_leg->leg_date                       = "info";
      $flight_leg->rtd                            = "info";
      $flight_leg->etd                            = "info";
      $flight_leg->atd                            = "info";
      $flight_leg->eta                            = "info";
      $flight_leg->ata                            = "info";
      $flight_leg->empty_seat_price               = "info";
      $flight_leg->airport_terminal_indications   = "info";
      $flight_leg->show_up_time                   = "info";
      $flight_leg->leg_capacity                   = "info";
      $flight_leg->id_arrival_airport             = "info";
      $flight_leg->id_departure_airport           = "info";
      // $flight_leg->id_flight_reports = "info";
      // $flight_leg->id_crew_members_list = "info";
      // $flight_leg->id_passengers_list = "info";
      $flight_leg->id_leg_type                    = "info";
      $flight_leg->id_flight                      = $flight->id;
      $flight_leg->save();

      $flight_charge = new Flight_Charge();
      $flight_charge->quoted_operator_price       = "info";
      $flight_charge->charger_by_operator         = "info";
      $flight_charge->actual_operator_price       = "info";
      $flight_charge->quoted_to_client            = "info";
      $flight_charge->charged_to_client           = "info";
      $flight_charge->quoted_taxes                = "info";
      $flight_charge->actual_taxes                = "info";
      $flight_charge->quoted_airport_taxes        = "info";
      $flight_charge->actual_airport_taxes        = "info";
      $flight_charge->paid_by_client              = "info";
      $flight_charge->paid_to_operator            = "info";
      $flight_charge->id_flight                   = $flight->id;
      $flight_charge->save();

      DB::commit(); //si nada fallo ejecuta los inserts
      $response = response()->json([
        'status'  => 'Created',
        'message' => 'El vuelo fue reservado exitosamente'
      ], 201);
    } catch (Exception $ex) {
      DB::rollback(); // borra el insert
      $response = response()->json([
        'status'  => 'Error',
        'message' => $ex->getMessage()
      ], 500);
    }
    return $response;
  }

  public function getAircrafts(Request $request)
  {
    // DECLARACIONES
    $user_logged = JWTAuth::user(); //user logged info
    $aircrafts = Aircraft::all(); //get all aircrafts in platform
    $ENV = Globals::find(1); //Envs
    $aircraftIsSuitable = false; //filter suitable
    $aircraft_data = []; //all aircraft_data
    $aircrafts_list = []; //Response
    $services = []; //aircrafts services
    $legs = [];
    $total_legs_distance = 0;
    $total_distance = 0;
    $partial_distance = 0;

    // INSTANCIANDO HELPER
    $calculate = new CustomsCalculated();

    //OBTENIENDO INFORMACION DEL FORMULARIO
    $params_array = [
      "flight_type"            => $request->input('flight_type'), //one_way, round_trip, multiple
      "type_operation"         => $request->input('type_operation'), //type_operation || medico o charter
      "legs"                   => $request->input('legs'), //[{arrive_airport, departure_airport, distance, date}]
      "region"                 => $request->input('region'), //id
      "passengers"             => $request->input('passengers'), //pasajeros
    ];
    $params = (object) $params_array;




    //MANEJANDO LISTADO DE AERONAVES
    if (count($aircrafts)) {
      // CONSTRUYENDO RESPUESTA DE LEGS
      //TODO AÑADIR LEGS DE BASE A SALIDA | DESTINO A BASE
      foreach ($params->legs as $leg) {
        $arrive_airport = Airport::where('id', $leg['arrive_airport'])->first();
        $departure_airport = Airport::where('id', $leg['departure_airport'])->first();
        $country_arrive = Country::where('code', $arrive_airport->iso_country)->first();
        $country_departure = Country::where('code', $departure_airport->iso_country)->first();

        $latitudeFrom = $departure_airport->latitude_deg;
        $longitudeFrom = $departure_airport->longitude_deg;
        $latitudeTo = $arrive_airport->latitude_deg;
        $longitudeTo = $arrive_airport->longitude_deg;
        $leg_distance = $calculate->getDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo);
        $__aircrafts  = [];

        $total_legs_distance += round($leg_distance, 2);

        $legs[] = [ //legs info
          "distance"            => round($leg_distance, 2), //? departure - arrival
          "date"                => $leg['date'], //la fecha debe ser superior a la fecha de hoyD
          "arrival_time"        => $leg['arrival_time'], //la fecha debe ser superior a la fecha de hoyD

          //* AEROPUERTO DE SALIDA
          "departure_airport"   => [
            "id_airport"        => $departure_airport->id,
            "airport_ident"     => $departure_airport->ident,
            "airport_name"      => $departure_airport->name,
            "id_country"        => $country_departure->id,
            "country_name"      => $country_departure->name,

            //distance
            "latitude_deg"      => $departure_airport->latitude_deg,
            "longitude_deg"     => $departure_airport->longitude_deg,

            //tasas
            "departure_taxes"   => $departure_airport->departure_taxes,
            "arrival_taxes"     => $departure_airport->arrival_taxes,
            "handling_services" => $departure_airport->handling_services,
          ],
          //* AEROPUERTO DE LLEGADA
          "arrive_airport" => [
            "id_airport"        => $arrive_airport->id,
            "airport_ident"     => $arrive_airport->ident,
            "airport_name"      => $arrive_airport->name,
            "id_country"        => $country_arrive->id,
            "country_name"      => $country_arrive->name,

            //distance
            "latitude_deg"      => $arrive_airport->latitude_deg,
            "longitude_deg"     => $arrive_airport->longitude_deg,

            //tasas
            "departure_taxes"   => $arrive_airport->departure_taxes,
            "arrival_taxes"     => $arrive_airport->arrival_taxes,
            "handling_services" => $arrive_airport->handling_services,
          ],
        ];
      }
      foreach ($aircrafts as $aircraft) {
        $aircraft_model = Aircraft_Model::where('id', $aircraft->id_aircraft_model)->first();
        $aircraft_airport = Airport::where('id', $aircraft->id_airport)->first(); //aircraft's airport

        $aircraft_data = (object) [
          "id_type_operation"   => $aircraft->id_type_operation,
          "available"           => $aircraft->available,
          "show_results"        => $aircraft->show_results,
          "pending_approval"    => $aircraft->pending_approval,
          "capacity"            => $aircraft->capacity === null ? 10 : $aircraft->capacity, //TODO PROVICIONAL
          "num_passengers"      => count($params->passengers),
          "range_value"         => $aircraft->range_value === null ? 3000 : $aircraft_model->range_value, //TODO PROVICIONAL
        ];

        //!DISTANCIAS
        //? DISTANCIAS BASE - ARRIVAL || DEPARTURE - BASE
        $base_to_departure = $calculate->getDistance(
          $aircraft_airport->latitude_deg,
          $aircraft_airport->longitude_deg,
          $legs[0]['departure_airport']['latitude_deg'],
          $legs[0]['departure_airport']['longitude_deg']
        );
        $base_from_arrival =
          $calculate->getDistance(
            $legs[0]['arrive_airport']['latitude_deg'],
            $legs[0]['arrive_airport']['longitude_deg'],
            $aircraft_airport->latitude_deg,
            $aircraft_airport->longitude_deg
          );

        $aircraftIsSuitable = $calculate->aircraftIsSuitable(
          $aircraft_data,
          $legs,
          $params->type_operation,
          $base_to_departure,
          $base_from_arrival,
          $ENV
        );

        //? REVISAR SI LA AERONAVE PUEDE PASAR POR EL AEROPUERTO
        $blacklist = Prohibited_Airports::where('id_aircraft', $aircraft->id)->get();
        $prohibed = $calculate->airportIsForbidden($params->legs, $blacklist);

        $departure_runaways = null;
        $arrive_runaways = null;

        foreach ($legs as $leg) {
          $departure_runaways = Airport_Rwy::where('airport_ident', $leg['departure_airport']['airport_ident'])->get(); //salida
          $arrive_runaways = Airport_Rwy::where('airport_ident', $leg['arrive_airport']['airport_ident'])->get(); //destino
        }

        $airports_runaways = array_merge($departure_runaways->toArray(), $arrive_runaways->toArray());

        $airport_suitable_aircraft = $calculate->airportisSuitableforAircraft($airports_runaways, $aircraft_model, $prohibed, $ENV);


        if ($airport_suitable_aircraft) {
          $assign_suitable_airport = $calculate->assignSuitableAirport(
            $legs,
            $aircraft_airport->latitude_deg,
            $aircraft_airport->longitude_deg,
            $ENV
          );
        }

        // $__aircrafts[] = [
        //   // "required_rwy_width"          => $aircraft_model->required_rwy_width,
        //   // "required_rwy_lenght"         => $aircraft_model->required_rwy_lenght,
        //   // "required_rwy_sfc"            => $aircraft_model->required_rwy_sfc,
        //   // "airports_runaways"           => $airports_runaways,
        //   // "airport_suitable_aircraft"   => $airport_suitable_aircraft,
        //   // "aircraftIsSuitable"          => $aircraftIsSuitable,
        //   // "assign_suitable_airport"     => $assign_suitable_airport,
        //   // "prohibed"                    => !$prohibed
        //   // "latitude_deg"                    => $aircraft_airport->latitude_deg,
        //   // "longitude_deg"                   => $aircraft_airport->longitude_deg,
        //   // "legs"                            => $legs,
        //   "assign_suitable_airport"         => $assign_suitable_airport,
        // ];
        // return $__aircrafts;


        if ($aircraftIsSuitable && !$prohibed && $airport_suitable_aircraft) {

          //? DETAILS OF AIRCRAFT
          $aircraft_detail = Aircraft_Detail::where('id', $aircraft->id_aircraft_detail)->first(); //detail
          $aircraft_services = Aircraft_Detail_Service::where('id_aircraft', $aircraft->id)->get(); //services
          $aircraft_group = Type_Aircraft_Group::where('id', $aircraft_model->id_aircraft_group)->first(); //type_group (Aircraft Size)
          $aircraft_costs = Aircraft_Cost_Info::where('id', $aircraft->id_aircraft_cost_info)->first(); //Cost info

          $aircraft_country = Country::where('code', $aircraft_airport->iso_country)->first(); //Country aircraft
          $airport_country = Country::where('code', $arrive_airport->iso_country)->first(); //Country airport

          $travel_distances = (object) [
            //? distances
            "base_to_departure"       => round($base_to_departure, 2),
            "base_from_arrival"       => round($base_from_arrival, 2),
            "total_legs_distance"     => round($total_legs_distance, 2),
            // "total_distance"          => $base_to_departure  + $total_legs_distance + $base_from_arrival,

            //? airports
            "base_airport"            => $aircraft_airport->id,
            "departure_airport"       => $legs[0]['departure_airport']['id_airport'],
            "arrive_airport"          => end($legs)['arrive_airport']['id_airport'],
          ];

          //? GET SERVICES
          foreach ($aircraft_services as $service) {
            $aircraft_service_level = Aircraft_Service_Level::where('id', $service->id_service)->first();
            $services[] = $aircraft_service_level;
          }

          //!PRECIOS

          //? Quoted_Operator_Price
          $aircraft_quoted_operator_price = (object) [
            'unit_used_km'                  => $aircraft_costs->unit_used_km,
            'has_segmented_prices'          => $aircraft_costs->has_segmented_prices,
            'price_1'                       => $aircraft_costs->price_1,
            'price_2'                       => $aircraft_costs->price_2,
            'price_3'                       => $aircraft_costs->price_3,
            'price_4'                       => $aircraft_costs->price_4,
            'price_5'                       => $aircraft_costs->price_5,
            'distance_th_1'                 => $aircraft_costs->distance_th_1,
            'distance_th_2'                 => $aircraft_costs->distance_th_2,
            'distance_th_3'                 => $aircraft_costs->distance_th_3,
            'distance_th_4'                 => $aircraft_costs->distance_th_4,
            'distance_th_5'                 => $aircraft_costs->distance_th_5,
            'min_trip_lenght'               => $aircraft_costs->min_trip_lenght,
            'min_distance'                  => $aircraft_costs->min_distance,
            'min_trip_price'                => $aircraft_costs->min_trip_price,

            // layover
            'id_country'                    => $aircraft_country->id,
            'national_layovers_cost'        => $aircraft_costs->national_layovers_cost,
            'international_layovers_cost'   => $aircraft_costs->international_layovers_cost,
            'airport_layover_1'             => $aircraft_airport->layover_1,
            'airport_layover_2'             => $aircraft_airport->layover_2,
            'airport_layover_3'             => $aircraft_airport->layover_3,
            'type_aircraft_groups'          => $aircraft_group->id,

            //medical cost
            'medical_cost_th'               => $aircraft_costs->medical_cost_th,
            'medical_cost_1'                => $aircraft_costs->medical_cost_1,
            'medical_cost_2'                => $aircraft_costs->medical_cost_2,

            //flight time
            'avg_speed_1'                   => $aircraft_costs->avg_speed_1,
            'avg_speed_2'                   => $aircraft_costs->avg_speed_2,
            'avg_speed_3'                   => $aircraft_costs->avg_speed_3,
            'avg_speed_4'                   => $aircraft_costs->avg_speed_4,
            'avg_speed_5'                   => $aircraft_costs->avg_speed_5,

          ];

          // get real distance by flight type
          $end_total_distance = $calculate->calculateTotalDistance(
            $travel_distances->total_legs_distance,
            $params->passengers,
            $params->type_operation,
            $params->flight_type,
            $ENV->DISTANCE_CORRECTION_COEFFICIENT
          ) + $travel_distances->base_to_departure + $travel_distances->base_from_arrival;

          $below_min_lenght_trips = $calculate->calculatebelowMinimumLenghtTrips(
            $aircraft_quoted_operator_price,
            $legs
          );

          $considered_distance = $calculate->calculateConsideredDistance(
            $aircraft_quoted_operator_price,
            $legs,
            round($end_total_distance, 2)
          );

          // // LEGS INFO
          $legs_distance = (object) [
            'travel_distances'        => $travel_distances,
            'considered_distance'     => $considered_distance,
            'total_distance'          => round($end_total_distance, 2),
            'average_distance'        => $calculate->calculateAverageLegDistance($legs, $considered_distance, $below_min_lenght_trips),
            'legs'                    => $legs,
          ];

          // quoted_operator_price
          $flight = (object)[
            'quoted_operator_price' => $calculate->calculateQuotedOperatorCost(
              $aircraft_quoted_operator_price,
              $legs_distance,
              $ENV->DISTANCE_1,
              count($params->passengers),
              $ENV->SUPERCHARGE_FACTOR_CORRECTION,
              $params->type_operation
            )
          ];

          //Tasas
          $aircraft_taxes = (object) [
            'id_country' => $airport_country->id,
            'national_taxes' => $aircraft_costs->national_taxes,
            'international_taxes' => $aircraft_costs->international_taxes
          ];

          // QUOTE PRICES
          $quoted_operator_taxes = $calculate->calculateTaxes($aircraft_taxes, $legs, $flight); //*LISTO
          $quoted_airport_taxes = $calculate->rates($legs, count($params->passengers), $params->flight_type);
          $actual_airport_taxes = $quoted_airport_taxes;
          $quoted_operator_price = $flight->quoted_operator_price;
          $quoted_margins = $calculate->calculateQuotedMargin($quoted_operator_price, 1, $legs[0]['date'], $ENV);
          $flight_time = $calculate->calculateFlightTime($aircraft_quoted_operator_price, $legs_distance);

          // TOTAL PRICE
          $precio_total = $calculate->totalChargedToClient(
            0,
            0,
            $quoted_operator_price,
            $quoted_margins
          );

          //! Creacion del vuelo
          $new_flight = new Flights();
          $new_flight->calculated_legs = count($legs);
          $new_flight->flight_date = $legs[0]['date'];
          $new_flight->caculated_flight_time = round($flight_time, 1);
          $new_flight->flight_condition = 1;
          $new_flight->entry_status = 6; //cotizacion
          $new_flight->id_departure_airport = $travel_distances->departure_airport;
          $new_flight->id_arrival_airport = $travel_distances->arrive_airport;
          $new_flight->id_aircraft = $aircraft->id;
          $new_flight->save();

          $flight_charges                             = new Flight_Charge();
          $flight_charges->quoted_operator_price      = $quoted_operator_price;
          $flight_charges->actual_operator_price      = $quoted_operator_price;
          $flight_charges->quoted_airport_taxes       = $quoted_airport_taxes;
          $flight_charges->actual_airport_taxes       = $quoted_airport_taxes;
          $flight_charges->id_flight                  = $new_flight->id;
          $flight_charges->charger_by_operator        = null;
          $flight_charges->quoted_to_client           = null;
          $flight_charges->charged_to_client          = null;
          $flight_charges->quoted_taxes               = null;
          $flight_charges->actual_taxes               = null;
          $flight_charges->paid_by_client             = null;
          $flight_charges->paid_to_operator           = null;
          $flight_charges->save();
          //! Fin Creacion de vuelo

          //Group Code by Aircraft group
          $aircrafts_list[$aircraft_group->name][] = [
            "flight_time"               => round($flight_time, 1),
            "total_price"               => round($precio_total, 2),
            "quoted_airport_taxes"      => $quoted_airport_taxes,
            "quoted_operator_taxes"     => round($quoted_operator_taxes, 2),
            "passenger_price"           => round($precio_total / count($params->passengers), 2),
            "flight_id"                 => $new_flight->id,
            "name"                      => $aircraft_model->name,
            "manufacturer"              => $aircraft_model->manufacter,
            "capacity"                  => $aircraft_data->capacity, //TODO provicional
            "picture_main"              => $aircraft_detail->picture_main,
            "services"                  => $services,
            "legs"                      => $legs,
            //! Debug
            // "flight"                                    => $flight,
            // "quoted_margins"                            => $quoted_margins,
            // "travel_distances"                          => $travel_distances,
            // "end_total_distance"                        => $end_total_distance,
            // "price_1"                                   => $aircraft_quoted_operator_price->price_1,
            // "min_distance"                              => $aircraft_quoted_operator_price->min_distance,
            // "aircraft_quoted_operator_price"            => $aircraft_quoted_operator_price,
            // "quoted_operator_taxes"                     => $quoted_operator_taxes,
            // "below_min_lenght_trips"                    => $below_min_lenght_trips,
            // "legs_distance"                             => $legs_distance,
            // "aircraft_taxes"                            => $aircraft_taxes,
            // "quoted_airport_taxes"                      => $quoted_airport_taxes,
            // "quoted_margins"                            => $quoted_margins,
            // "total_legs_distance"                       => $total_legs_distance,
            // "considered_distance"                       => $considered_distance,
            // "travel_distances"                          => $travel_distances,
            // "aircraft_quoted_operator_price"            => $aircraft_quoted_operator_price,
            // "legs"                                      => $legs,
            //! Range
            // "aircraft_data"                                 => $aircraft_data,
            // "aircraftIsWithinRange"                         => $calculate->aircraftIsWithinRange(
            //   $legs,
            //   $aircraft_data->range_value,
            //   $base_to_departure,
            //   $base_from_arrival,
            //   $ENV->STOPS_AIRCRAFT_RANGE
            // ),
            //! Runaways
            // "airports_runaways"                                      => $airports_runaways,
          ];
        }
      }
    }
    if (count($aircrafts_list)) {
      $response = response()->json($aircrafts_list, 200); //lista de aeronaves priorizadas
    } else {
      $response =
        response()->json([
          "status"    => "No content",
          "message"   => "No se han encontrado registros",
        ], 404);
    }
    // return $response;
    return $response;
  }

  public function getAirports($query)
  {

    $data = [];

    // $aircraft_data = [];
    // $aircrafts_list = [];
    $regions = Region::where('name', 'LIKE', "%$query%")->get();
    $airports = Airport::where('name', 'LIKE', "%$query%")->take(50)->get();
    if (count($regions)) {
      foreach ($regions as $region) {
        $airports = Airport::where('iso_region', $region->code)->get();
        $data[] = [
          "region" => $region,
          "airport" => $airports,
        ];
        $response = response()->json($data, 200);
      }
    } else if (count($airports)) {
      $response = response()->json($airports, 200);
      foreach ($airports as $airport) {
        $region = Region::where('code', $airport->iso_region)->first();
        $data[] = [
          "region" => $region,
          "airport" => $airports,
        ];
        $response = response()->json($data, 200);
      }
    } else {
      $response = response()->json([
        'status'    => 'Errors',
        'message'   => 'No se han encontrado resultados a su busqueda'
      ], 204);
    }
    return $response;
  }

  public function idaQuote(Request $request)
  {
    $json = $request->input('json', null);

    $params = json_decode($json);
    $params_array = json_decode($json, true);

    if (!empty($params) && !empty($params_array)) {

      //Validamos los datos
      $validate = Validator::make($params_array, [
        'airport_depart'     => 'required',
        'airport_arrive'     => 'required',
        'depart_date'        => 'required',
        'number_passengers'  => 'required'
      ]);

      if ($validate->fails()) {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validation errors',
          'message'   => 'Ha ocurrido un problema',
          'errors'    => $validate->errors()
        ), 404);
      } else {

        // Logica del cotizador de "Solo Ida" aquí
        $response = response()->json(array(
          'status'    => 'Success',
          'message'   => 'Servicio cotizado correctamente'
        ), 200);
      }
    } else { // Si los datos están vacíos
      $response = response()->json(array(
        'status'    => 'Empty request',
        'message'   => 'No se han enviado datos en la petición'
      ), 404);
    }
    return $response;
  }

  public function idaVueltaQuote(Request $request)
  {
    $json = $request->input('json', null);

    $params = json_decode($json);
    $params_array = json_decode($json, true);

    if (!empty($params) && !empty($params_array)) {

      //Validamos los datos
      $validate = Validator::make($params_array, [
        'airport_depart'     => 'required',
        'airport_arrive'     => 'required',
        'depart_date'        => 'required',
        'number_passengers'  => 'required'
      ]);

      if ($validate->fails()) {
        // Validaciones fallan
        $response = response()->json(array(
          'status'    => 'Validations errors',
          'message'   => 'Ha ocurrido un problema',
          'errors'    => $validate->errors()
        ), 400);
      } else {

        // Logica del cotizador de "Ida y Vuelta" aquí
        $response = response()->json(array(
          'status'    => 'Success',
          'message'   => 'Servicio cotizado correctamente'
        ), 200);
      }
    } else { // Si los datos están vacíos
      $response = response()->json(array(
        'status'    => 'Empty resources',
        'message'   => 'Datos incompletos'
      ), 400);
    }
    return $response;
  }

  public function getFlightsByDate(Request $request)
  {
    $params_array = [
      //FLIGHT
      "flight_date_going"       => ('2020-02-29'),
      "flight_date_return"      => ('2020-04-29'),
      // "flight_date_going"       => $request->input('flight_date_going'),
      // "flight_date_return"      => $request->input('flight_date_return'),
      //FLIGHT_LEG
      //   "leg_capacity"            => $request->input('leg_capacity'), 
      //   "empty_seat_price"        => $request->input('empty_seat_price'),
      //AIRPORTS
      //   "Destination"         => $request->input('Destination'), //deben ser hasta 5, foreach?
    ];

    $params = (object) $params_array;

    $validate = Validator::make($params_array, [
      'flight_date_going'  => 'required',
      // 'Destination'        => 'required',
    ]);
    if (!$validate->fails()) {

      // $quote = Flights::where('flight_date',$request->date)->get();

      $flightsList = Flights::whereBetween(
        'flight_date',
        //comparo la fecha de salida con la fecha de regreso.
        [$params->flight_date_going, $params->flight_date_return]
      )
        //ordeno de fecha mas cercana a mas lejana.
        ->orderBy('flight_date', 'ASC')->get();

      $response = response()->json(array(
        'status'    => 'Success',
        'flights' => $flightsList
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Validation Error',
        'message'   => 'Ha ocurrido un problema con la validación de los datos',
        'errors'    => $validate->errors()
      ), 400);
    }
    return $response;
  }

  public function bookFlight(Request $request)
  {
    $params_array = [
      "percent"                 => $request->input('percent'), //porcentaje de pago
      "init"                    => $request->input('init'), //salida
      "end"                     => $request->input('end'), //destino
      "type"                    => $request->input('type'), //si es oneway o round trip
      // flight data
      "flight_date"             => $request->input('flight_date'),
      "flight_condition"        => $request->input('flight_condition'),
      "calculated_legs"         => $request->input('calculated_legs'),
      "caculated_flight_time"   => $request->input('caculated_flight_time'),
      "entry_status"            => $request->input('entry_status'),
    ];
    $params = (object) $params_array; //*parse to object
    $validate = Validator::make($params_array, [
      'percent'                 => 'required',
      'init'                    => 'required',
      'end'                     => 'required',
      'type'                    => 'required',
      // flight data
      "flight_date"             => '',
      "flight_condition"        => '',
      "calculated_legs"         => '',
      "caculated_flight_time"   => '',
      "entry_status"            => '',
    ]);
    if (!$validate->fails()) { //*validation pass

      //TODO Falta crear el vuelo pero nose cuales son los parametros
      $global = Globals::where('id', 1)->first();

      $range = 500; //*variable global
      $distancia = 500;
      $average_speed = 1; //*1, 2, 3, 4

      $distancia_total = $params->init + $params->end; //*distancia entre punto init y end
      $coeficiente_correccion_rango = $global->coeficiente_correcion_rango; //*variable global
      $flight_time = 30 + ($distancia / $average_speed); //*tiempo de vuelo;

      $flight = new Flights();
      $flight->flight_date = $params->flight_date;
      $flight->flight_condition = $params->flight_condition;
      $flight->calculated_legs = $params->calculated_legs;
      $flight->caculated_flight_time = $flight_time;
      $flight->entry_status = $params->entry_status;
      $flight->save();

      if ($distancia_total > $range * $coeficiente_correccion_rango && ($params->type === "oneway" || $params->type === "round_trip")) {
        $response = response()->json(array(
          'status'    => 'Success',
          'messages'   => array(
            'Este viaje puede necesitar una o más escalas técnicas, sin cambio de avión',
            'El tiempo de vuelo promedio será ' . $flight_time
          )
        ), 200);
      } else if ($distancia_total > $range * $coeficiente_correccion_rango) {
        $response = response()->json(array(
          'status'    => 'Success',
          'messages'   => array(
            'Este viaje puede necesitar una o más escalas técnicas, sin cambio de avión'
          )
        ), 200);
      } else {
        $response = response()->json(array(
          'status'    => 'Success',
          'messages'   => array(
            'El tiempo de vuelo promedio será ' . $flight_time
          )
        ), 200);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Validation Error',
        'message'   => 'Ha ocurrido un problema con la validación de los datos',
        'errors'    => $validate->errors()
      ), 400);
    }
    return $response;
  }

  public function getAircraftsByAirport($id)
  {
    $aircrafts = Aircraft::where('id_airport', $id)->get();
    $aircraft_data = [];
    if (count($aircrafts)) {
      foreach ($aircrafts as $aircraft) {
        $aircraft_data[] = [
          "aircraft"                    => $aircraft,
          "aircraft_detail"             => $aircraft->aircraft_detail,
          "type_of_operation"           => $aircraft->type_of_operation,
          "aircraft_model"              => $aircraft->aircraft_model,
        ];
      }
      $response = response()->json($aircraft_data, 200);
    } else {
      $response = response()->json([
        "status"    => "No content",
        "message"   => "No se han encontrado aeronaves para ese aeropuerto",
      ], 204);
    }
    return $response;
  }

  public function flightRequest($id)
  {
    // Notificar a admin 1
    // Notificar a admin 2
    // notificar a admin 3 (debe confirmar el precio)
  }

  public function flightConfirm(Request $request, $id)
  {
    $flight = Flights::where('id', $id)->first(); //vuelos
    $flight_charge = Flight_Charge::where('id_flight', $flight->id)->first(); //precios del 
    $user_logged = JWTAuth::user();


    if (is_object($flight) || is_object($flight_charge)) {
      if ($user_logged->id_level < 3 || ($user_logged->id_level === 4 || $user_logged->id_level === 5)) { //confirmar permisos

        $params_array = [
          'entry_status'              => $request->input('entry_status'), //aceptado || rechazado
        ];
        $params = (object) $params_array; //convirtiendo en obj

        $flight->entry_status = $params->entry_status; //nose si va en este punto

        if ($params->entry_status === true) {
          if (($flight_charge->actual_operator_price <= $flight_charge->quoted_operator_price) || ($flight_charge->actual_airport_taxes <= $flight_charge->quoted_airport_taxes) || ($flight_charge->actual_taxes <= $flight_charge->quoted_taxes)) {

            //crear el vuelo
            $flight = new Flights();
            $flight->update();
            // añadir pasajero al requested leg

            //notificar a 1 y 2
            $admins = User::where('id_level', "<=", 2)->get();
            foreach ($admins as $admin) {
              $notification = new Notifications();
              $notification->type = "";
              $notification->description = 'El vuelo ID ' . $flight->id . ' ha sido aceptado por el operador ' . $user_logged->username;
              $notification->id_user_transmitter = $user_logged->id_user;
              $notification->id_user_receiver = $admin->id; //admin 1, 2
              $notification->status = false;
            }

            //"El vuelo ID XXX ha sido aceptado por el operador $user_logged->first_name"
            $response = response()->json(array(
              'status'    => 'Success',
              'message'   => 'El vuelo ID ' . $flight->id . ' ha sido aceptado por el operador' . $user_logged->username,
            ), 200);
          }
        } else {
          //notificar a 1 y 2
          //"El vuelo ID XXX ha sido rechazado por el operador $user_logged->first_name"
          $response = response()->json(array(
            'status'    => 'Success',
            'message'   => 'El vuelo ID ' . $flight->id . ' ha sido rechazada por el operador' . $user_logged->username,
          ), 200);
        }
      } else {
        $response = response()->json(array(
          'status'    => 'Forbidden',
          'message'   => 'No tiene los permisos para ejecutar esta accion',
        ), 403);
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Validation Error',
        'message'   => 'Ha ocurrido un problema para encontrar le vuelo',
      ), 204);
    }
    return $response;
  }
}
