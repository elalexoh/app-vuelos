<?php

namespace App\Http\Controllers;
use App\Mail\Contact;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contactMail(Request $request)
    {

     // $mail = $request->input('mail');
    $mail = ' anthony-gadea@hotmail.com';

    Mail::to($mail)->send(new Contact()); 

    return 'A message has been sent to'.$mail;
    }
}
