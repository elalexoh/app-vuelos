<?php

namespace App\Helpers;

use Firebase\JWT\JWT;
use App\Models\User;
use App\Models\Level;
use Illuminate\Support\Facades\DB;

class JwtAuth
{

  public $key;

  public function __construct()
  {
    $this->key = 'AppVuelos';
  }

  public function signup($username, $password, $getToken = null)
  {

    // Buscar si existe el usuario con las credenciales
    $usuario = User::where('username', $username)->first();
    $response = "";

    if (is_object($usuario)) {
      $level = Level::find($usuario->id_level);

      // Validación de contraseña correcta
      if ($usuario->password != $password) {

        $response = response()->json(
          array(
            'status'    => 'Unauthorized',
            'message'   => 'Ha ocurrido un problema con sus datos, verifique su información e intente nuevamente'
          ),
          401
        );
      } else {

        $token = array(
          'id_user'   =>  $usuario->id,
          'username'  =>  $usuario->username,
          'id_level'  =>  $level->id,
          'level'     =>  $level->name,
          'iat'       =>  time(),
          'exp'       =>  time() + (24 * 60 * 60) // Una semana de duración.
        );

        $jwt = JWT::encode($token, $this->key, 'HS256');  // La key es una clave que posee el desarrollador
        $decoded = JWT::decode($jwt, $this->key, ['HS256']);

        // Devolver los datos decodificados o el token, en función de un parámetro
        if (is_null($getToken)) {
          $response = response()->json($jwt, 200);
        } else {
          $response = response()->json($decoded, 200);
        }
      }
    } else {
      $response = response()->json(array(
        'status'    => 'Unauthorized',
        'message'   => 'Esta cuenta no esta registrada en la plataforma'
      ), 401);
    }

    return $response;
  }

  public function checkToken($jwt, $getIdentity = false)
  {

    // Se usará en las distintas acciones para verificar el token del usuario logueado.

    $auth = false;

    try {
      $jwt = str_replace('"', '', $jwt);
      $decoded = JWT::decode($jwt, $this->key, ['HS256']);
    } catch (\UnexpectedValueException $e) {
      $auth = false;
    } catch (\DomainException $e) {
      $auth = false;
    }

    if (!empty($decoded) && is_object($decoded) && isset($decoded->id_user)) {
      $auth = true;
    } else {
      $auth = false;
    }

    if ($getIdentity) {
      return $decoded;
    }

    return $auth;
  }
}
