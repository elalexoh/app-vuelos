<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_of_Operation extends Model
{
    protected   $table = 'type_of_operation';
    public      $timestamps = false;
    
    //Relacion de uno a muchos con la tabla aircraft
    public function aircraft(){
        return $this->hasMany('App\Models\Aircraft' ,'fk_type_operation');
    }
}
