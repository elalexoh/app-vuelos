<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote_Arrive extends Model
{
  protected   $table = 'quote_arrive';
  public      $timestamps = false;

  //Relacion de muchos a uno con la tabla quote
  public function quote()
  {
    return $this->belongsTo('App\Models\Quote', 'id_quote');
  }

  //Relacion de muchos a uno con la tabla region
  public function region()
  {
    return $this->belongsTo('App\Models\Region', 'id_region');
  }
}
