<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
  protected $fillable = [
    "id",
    "code",
    "local_code",
    "name",
    "continent",
    "iso_country",
    "wikipedia_link",
    "keywords"
  ];
  protected   $table = 'region';
  public      $timestamps = false;

  //Relacion de muchos a uno con la tabla countrie
  public function country()
  {
    return $this->belongsTo('App\Models\Country', 'id_countrie');
  }

  //Relacion de uno a muchos con la tabla aircraft
  public function airport()
  {
    return $this->hasMany('App\Models\Airport', 'id_region');
  }

  //Relacion de muchos a uno con la tabla region
  public function quote()
  {
    return $this->hasMany('App\Models\Quote', 'id_region_depart');
  }

  //Relacion muchos a muchos
  public function quote_arrive()
  {
    return $this->belongsToMany('App\Models\Quote', 'quote_arrive', 'id_region', 'id_quote');
  }
}
