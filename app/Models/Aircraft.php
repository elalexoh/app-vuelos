<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
  protected   $table = 'aircraft';
  public      $timestamps = false;

  //Relacion de muchos a uno con la tabla aircraft_model
  public function aircraft_model()
  {
    return $this->belongsTo('App\Models\Aircraft_Model', 'id_aircraft_model');
  }

  //Relacion de muchos a uno con la tabla users
  public function users()
  {
    return $this->belongsTo('App\Models\User', 'id_user_owner');
  }

  //Relacion de muchos a uno con la tabla airports
  public function airport()
  {
    return $this->belongsTo('App\Models\Airport', 'id_airport');
  }

  //Relacion de muchos a uno con la tabla airports
  public function prohibited_airports()
  {
    return $this->belongsTo('App\Models\ProhibitedAirports', 'id_prohibited_airports');
  }

  //Relacion de muchos a uno con la tabla aircraft_detail
  public function aircraft_detail()
  {
    return $this->belongsTo('App\Models\Aircraft_Detail', 'id_aircraft_detail');
  }

  //Relacion de muchos a uno con la tabla aircraft_cost_info
  public function aircraft_cost_info()
  {
    return $this->belongsTo('App\Models\Aircraft_Cost_Info', 'id_aircraft_cost_info');
  }

  //Relacion de muchos a uno con la tabla type_of_operation
  public function type_of_operation()
  {
    return $this->belongsTo('App\Models\Type_Operation', 'id_type_operation');
  }
}
