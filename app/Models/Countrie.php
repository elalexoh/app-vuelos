<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countrie extends Model
{
    protected   $table = 'countrie';
    public      $timestamps = false;

    //Relacion de uno a muchos con la tabla region
    public function region(){
        return $this->hasMany('App\Models\Region' ,'fk_countrie');
    }
    
    //Relacion de uno a muchos con la tabla passport
    public function passport(){
        return $this->hasMany('App\Models\Passport' ,'fk_countrie');
    }
    
    //Relacion de uno a muchos con la tabla company
    public function company(){
        return $this->hasMany('App\Models\Company' ,'fk_countrie');
    }
    
    //Relacion de uno a muchos con la tabla personal_information
    public function personal_information(){
        return $this->hasMany('App\Models\Personal_Information' ,'fk_countrie');
    }

    //Relacion de muchos a uno con la tabla continent
    public function continent(){
        return $this->belongsTo('App\Models\Continent' ,'fk_continent');
    }
}
