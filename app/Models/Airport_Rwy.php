<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Airport_Rwy extends Model
{
  protected   $fillable = [
    'id',
    'id_airport',
    'id_header',
    'lenght_ft',
    'width_ft',
    'surface',
    'lighted',
    'closed',
    'le_ident',
    'le_latitude_deg',
    'le_longitude_deg',
    'le_elevation_ft',
    'le_heading_degT',
    'le_displaced_threshold_ft',
    'he_ident',
    'he_latitude_deg',
    'he_longitude_deg',
    'he_elevation_ft',
    'he_heading_degT',
    'he_displaced_threshold_ft',
    'airport_ident',
  ];
  protected   $table = 'airport_rwy';
  public      $timestamps = false;

  public function airport()
  {
    return $this->belongsTo('App\Models\Airport', 'ref');
  }
}
