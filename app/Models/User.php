<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements JWTSubject
{
  protected   $table = 'users';
  // public      $timestamps = false;

  protected $fillable = [
    'username', 'password', 'created_date', 'modified_date'
  ];

  //Relacion de muchos a uno con la tabla crewmember_detail
  public function crewmember_detail()
  {
    return $this->belongsTo('App\Models\Crewmember_Detail', 'id_crewmember_detail');
  }

  //Relacion de muchos a uno con la tabla level
  public function level()
  {
    return $this->belongsTo('App\Models\Level', 'id_level');
  }

  //Relacion de uno a muchos con la tabla reset_pass
  public function reset_pass()
  {
    return $this->hasMany('App\Models\Reset_Pass', 'id_user');
  }

  //Relacion de muchos a uno con la tabla personal_information
  public function personal_information()
  {
    return $this->belongsTo('App\Models\Personal_Information', 'id_personal_information');
  }

  //Relacion de muchos a uno con la tabla user
  public function user1()
  {
    return $this->hasMany('App\Models\User', 'id_user_creator');
  }

  //Relacion de muchos a uno con la tabla user
  public function user2()
  {
    return $this->belongsTo('App\Models\User', 'id_user_creator');
  }

  //Relacion de uno a muchos con la tabla aircraft
  public function aircraft()
  {
    return $this->hasMany('App\Models\Aircraft', 'id_user_owner');
  }

  //Relacion de uno a muchos con la tabla quote
  public function quote()
  {
    return $this->hasMany('App\Models\Quote', 'id_user');
  }

  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  public function getJWTCustomClaims()
  {
    return [];
  }
}
