<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aircraft_Cost_Info extends Model
{
  protected   $table = 'aircraft_cost_info';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla aircraft
  public function aircraft()
  {
    return $this->hasMany('App\Models\Aircraft', 'id_aircraft_cost_info');
  }
}
