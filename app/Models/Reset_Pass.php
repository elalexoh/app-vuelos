<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reset_Pass extends Model
{
  protected   $table = 'reset_pass';
  public      $timestamps = false;

  //Relacion de muchos a uno con la tabla user
  public function user()
  {
    return $this->belongsTo('App\Models\User', 'id_user');
  }
}
