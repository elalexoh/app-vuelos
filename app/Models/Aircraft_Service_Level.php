<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aircraft_Service_Level extends Model
{
  protected   $table = 'aircraft_service_level';
  public      $timestamps = false;

  //Relacion muchos a muchos
  public function aircraft_detail()
  {
    return $this->belongsToMany('App\Models\Aircraft_Detail', 'aircraft_detail_service', 'id_service', 'id_aircraft');
  }
}
