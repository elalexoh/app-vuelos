<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crewmember_Detail extends Model
{
  protected   $table = 'crewmember_detail';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla user
  public function user()
  {
    return $this->hasMany('App\Models\User', 'id_crewmember_detail');
  }
}
