<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aircraft_Model extends Model
{
  protected $fillable = [
    'id',
    'manufacter',
    'name',
    'type_designator',
    'description',
    'engine_type',
    'engine_count',
    'wtc',
    'shown_name',
    'aircraft_group',
    'capacity',
    'max_range_nm',
    'max_elev_isa',
    'avg_speed',
    'required_rwy_sfc',
    'required_rwy_width',
    'required_rwy_lenght',
    'side_picture',
    'default_picture1',
    'default_picture2'
  ];
  protected   $table = 'aircraft_model';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla aircraft
  public function aircraft()
  {
    return $this->hasMany('App\Models\Aircraft', 'id_aircraft_model');
  }

  //Relacion de muchos a uno con la tabla type_of_aircraft_group
  public function type_of_aircraft_group()
  {
    return $this->belongsTo('App\Models\Type_Aircraft_Group', 'id_aircraft_group');
  }
}
