<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_Aircraft_Group extends Model
{
  protected   $table = 'type_aircraft_group';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla aircraft_model
  public function aircraft_model()
  {
    return $this->hasMany('App\Models\Aircraft_Model', 'id_aircraft_group');
  }
}
