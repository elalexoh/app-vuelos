<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
  protected $fillable = [
    'id',
    'ref',
    'ident',
    'name',
    'latitude_deg',
    'longitude_deg',
    'elevation_ft',
    'continent',
    'iso_country',
    'iso_region',
    'municipality',
    'scheduled_service',
    'gps_code',
    'iata_code',
    'local_code',
    'home_link',
    'wikipedia_link',
    'keywords',
  ];
  protected   $table = 'airport';
  public      $timestamps = false;

  //Relacion de muchos a uno con la tabla region
  public function region()
  {
    return $this->belongsTo('App\Models\Region', 'id_region');
  }

  //Relacion de uno a muchos con la tabla aircraft
  public function aircraft()
  {
    return $this->hasMany('App\Models\Aircraft', 'id_airport');
  }

  //Relacion de uno a muchos con la tabla airport_rwy
  public function airport_rwy()
  {
    return $this->hasMany('App\Models\Airport_Rwy', 'id_airport');
  }
}
