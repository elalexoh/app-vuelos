<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
  protected   $table = 'levels';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla user
  public function user()
  {
    return $this->hasMany('App\Models\User', 'id_level');
  }
}
