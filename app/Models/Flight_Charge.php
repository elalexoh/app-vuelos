<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flight_Charge extends Model
{
  protected   $table = 'flight_charges';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla Flights
  public function flights()
  {
    return $this->belongsTo('App\Models\Country', 'id_flights');
  }
}
