<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aircraft_Detail extends Model
{
  protected   $table = 'aircraft_detail';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla aircraft
  public function aircraft()
  {
    return $this->hasMany('App\Models\Aircraft', 'id_aircraft_detail');
  }

  //Relacion muchos a muchos
  public function aircraft_service_level()
  {
    return $this->belongsToMany('App\Models\Aircraft_Service_Level', 'aircraft_detail_service', 'id_aircraft', 'id_service');
  }
  public function forbiden_airports()
  {
    return $this->hasMany('App\Models\Airports', 'id_prohibited_airports');
  }
}
