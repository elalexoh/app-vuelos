<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
  protected   $table = 'quote';
  public      $timestamps = false;

  //Relacion de muchos a uno con la tabla users
  public function user()
  {
    return $this->belongsTo('App\Models\User', 'id_user');
  }

  //Relacion de muchos a uno con la tabla region
  public function region()
  {
    return $this->belongsTo('App\Models\Region', 'id_region_depart');
  }

  //Relacion muchos a muchos
  public function region_arrive()
  {
    return $this->belongsToMany('App\Models\Region', 'quote_arrive', 'id_quote', 'id_region');
  }
}
