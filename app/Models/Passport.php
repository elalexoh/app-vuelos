<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Passport extends Model
{
  protected   $table = 'passport';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla personal_information
  public function personal_information()
  {
    return $this->hasMany('App\Models\Personal_Information', 'id_passport');
  }

  //Relacion de muchos a uno con la tabla countrie
  public function countrie()
  {
    return $this->belongsTo('App\Models\Country', 'id_countrie');
  }
}
