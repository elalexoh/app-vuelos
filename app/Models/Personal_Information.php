<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personal_Information extends Model
{
  protected   $table = 'personal_information';
  // public      $timestamps = false;

  //Relacion de uno a muchos con la tabla user
  public function user()
  {
    return $this->hasMany('App\Models\User', 'id_personal_information');
  }

  //Relacion de muchos a uno con la tabla countrie
  public function countrie()
  {
    return $this->belongsTo('App\Models\Country', 'id_countrie');
  }

  //Relacion de muchos a uno con la tabla company
  public function company()
  {
    return $this->belongsTo('App\Models\Company', 'id_company');
  }

  //Relacion de muchos a uno con la tabla passport
  public function passport()
  {
    return $this->belongsTo('App\Models\Passport', 'id_passport');
  }
}
