<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prohibited_Airports extends Model
{
  /**
   * Run the database seeds.
   *
   * @return void
   */

  protected   $table = 'prohibited_airports';
  public      $timestamps = false;
}
