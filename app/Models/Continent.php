<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Continent extends Model
{
  protected   $table = 'continent';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla countrie
  public function countrie()
  {
    return $this->hasMany('App\Models\Country', 'id_continent');
  }
}
