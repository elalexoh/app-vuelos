<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aircraft_Detail_Service extends Model
{
  protected   $table      = 'aircraft_detail_service';
  public      $timestamps = false;

  //Relacion de muchos a uno
  public function aircraft_detail()
  {
    return $this->belongsTo('App\Models\Aircraft_Detail', 'id_aircraft');
  }

  //Relacion de muchos a uno
  // public function aircraft_service_level()
  // {
  //   return $this->hasMany('App\Models\Aircraft_Service_Level');
  // }
  public function aircraft_service_level()
  {
    return $this->belongsTo('App\Models\Aircraft_Service_Level', 'id_service', 'aircraft_service_level');
  }
}
