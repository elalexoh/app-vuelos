<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  protected   $table = 'company';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla personal_information
  public function personal_information()
  {
    return $this->hasMany('App\Models\Personal_Information', 'id_company');
  }

  //Relacion de muchos a uno con la tabla countrie
  public function countrie()
  {
    return $this->belongsTo('App\Models\Country', 'id_countrie');
  }
}
