<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  protected $fillable = [
    "id",
    "code",
    "name",
    "continent",
    "wikipedia_link",
    "keywords"
  ];
  protected   $table = 'country';
  public      $timestamps = false;

  //Relacion de uno a muchos con la tabla region
  public function region()
  {
    return $this->hasMany('App\Models\Region', 'id_country');
  }

  //Relacion de uno a muchos con la tabla passport
  public function passport()
  {
    return $this->hasMany('App\Models\Passport', 'id_country');
  }

  //Relacion de uno a muchos con la tabla company
  public function company()
  {
    return $this->hasMany('App\Models\Company', 'id_country');
  }

  //Relacion de uno a muchos con la tabla personal_information
  public function personal_information()
  {
    return $this->hasMany('App\Models\Personal_Information', 'id_country');
  }

  //Relacion de muchos a uno con la tabla continent
  public function continent()
  {
    return $this->belongsTo('App\Models\Continent', 'id_continent');
  }
}
