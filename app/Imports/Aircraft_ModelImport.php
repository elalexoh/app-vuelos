<?php

namespace App\Imports;

use App\Models\Aircraft_Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class Aircraft_ModelImport implements ToModel, WithChunkReading
{
  /**
   * @param array $row
   *
   * @return \Illuminate\Database\Eloquent\Model|
   */
  public function model(array $row)
  {
    // id,0
    // manufacturer,1
    // name,2
    // type_designator,3
    // description,4
    // engine type,5
    // engine count,6
    // wtc,7
    // shown_name,8
    // id_aircraft_group,9
    // capacity,10
    // max_range_nm,11
    // average_speed,12
    // required_rwy_sfc,13
    // required_rwy_width,14
    // required_rwy_lenght,15
    return new Aircraft_Model([
      'id'                  => $row[0],
      'manufacturer'        => $row[1],
      'name'                => $row[2],
      'type_designator'     => $row[3],
      'description'         => $row[4],
      'engine_type'         => $row[5],
      'engine_count'        => $row[6],
      'wtc'                 => $row[7],
      'shown_name'          => $row[8],
      'id_aircraft_group'   => $row[9],
      'capacity'            => $row[10],
      'max_range_nm'        => $row[11],
      'avg_speed'           => $row[12],
      'required_rwy_sfc'    => $row[13],
      'required_rwy_width'  => $row[14],
      'required_rwy_lenght' => $row[15],
      'max_elev_isa'        => 10000,
      // 'side_picture'        => $row[17], 
      // 'default_picture1'    => $row[18], 
      // 'default_picture2'    => $row[19], 

    ]);
  }
  public function chunkSize(): int
  {
    return 5000;
  }
}
