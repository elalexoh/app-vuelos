<?php

namespace App\Imports;

use App\Models\Airport;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class AirportImport implements ToModel, WithChunkReading
{
  /**
   * @param array $row
   *
   * @return \Illuminate\Database\Eloquent\Model|null
   */
  public function model(array $row)
  {
    return new Airport([
      'ref'                 => $row[0],
      'ident'               => $row[1],
      'name'                => $row[3],
      'latitude_deg'        => $row[4],
      'longitude_deg'       => $row[5],
      'elevation_ft'        => $row[6],
      'continent'           => $row[7],
      'iso_country'         => $row[8],
      'iso_region'          => $row[9],
      'municipality'        => $row[10],
      'scheduled_service'   => $row[11],
      'gps_code'            => $row[12],
      'iata_code'           => $row[13],
      'local_code'          => $row[14],
      'home_link'           => $row[15],
      'wikipedia_link'      => $row[16],
      'keywords'            => $row[17],
    ]);
  }

  public function chunkSize(): int
  {
    return 5000;
  }
}
