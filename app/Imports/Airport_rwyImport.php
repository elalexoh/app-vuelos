<?php

namespace App\Imports;

use App\Models\Airport_rwy;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class Airport_rwyImport implements ToModel, WithChunkReading
{
  /**
   * @param array $row
   *
   * @return \Illuminate\Database\Eloquent\Model|null
   */
  public function model(array $row)
  {
    return new Airport_rwy([
      'id' => $row[0],
      'airport_ref' => $row[1],
      'airport_ident' => $row[2],
      'lenght_ft' => $row[3],
      'width_ft' => $row[4],
      'surface' => $row[5],
      'lighted' => $row[6],
      'closed' => $row[7],
      'le_ident' => $row[8],
      'le_latitude_deg' => $row[9],
      'le_longitude_deg' => $row[10],
      'le_elevation_ft' => $row[11],
      'le_heading_degT' => $row[12],
      'le_displaced_threshold_ft' => $row[13],
      'he_ident' => $row[14],
      'he_latitude_deg' => $row[15],
      'he_longitude_deg' => $row[16],
      'he_elevation_ft' => $row[17],
      'he_heading_degT' => $row[18],
      'he_displaced_threshold_ft' => $row[19],
    ]);
  }
  public function chunkSize(): int
  {
    return 5000;
  }
}
