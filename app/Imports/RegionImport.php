<?php

namespace App\Imports;

use App\Models\Region;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class RegionImport implements ToModel, WithChunkReading
{
  /**
   * @param array $row
   *
   * @return \Illuminate\Database\Eloquent\Model|null
   */
  public function model(array $row)
  {
    return new Region([
      "id" => $row[0],
      "code" => $row[1],
      "local_code" => $row[2],
      "name" => $row[3],
      "continent" => $row[4],
      "iso_country" => $row[5],
      // "wikipedia_link" => $row[6],
      // "keywords" => $row[7]
    ]);
  }
  public function chunkSize(): int
  {
    return 5000;
  }
}
