<?php

namespace App\Imports;

use App\Models\Country;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class CountryImport implements ToModel, WithChunkReading
{
  /**
   * @param array $row
   *
   * @return \Illuminate\Database\Eloquent\Model|null
   */
  public function model(array $row)
  {
    // 302672,"AD","Andorra","EU","https://en.wikipedia.org/wiki/Andorra",
    return new Country([
      "id" => $row[0],
      "code" => $row[1],
      "name" => $row[2],
      "id_continent" => $row[3],
      "wikipedia_link" => $row[4],
      "keywords" => $row[5],
    ]);
  }

  public function chunkSize(): int
  {
    return 5000;
  }
}
