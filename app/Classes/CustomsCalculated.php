<?php

namespace App\Classes;

use PhpParser\Node\Expr\Cast\Object_;
use Prophecy\Promise\ReturnPromise;
use SebastianBergmann\Type\ObjectType;

class CustomsCalculated
{

  //!Priorizacion de aeronaves
  /**
   * Check Aircraft is suitable
   * @param object $aircraft
   * @param object $legs
   * @param integer $type_operation
   * @param float|int $base_to_departure
   * @param float|int $base_from_arrival
   * @param object $ENV
   * @return boolean
   */
  public function aircraftIsSuitable(
    $aircraft,
    $legs,
    $type_operation,
    $base_to_departure,
    $base_from_arrival,
    $ENV
  ) {
    $isCorrectTypeOfOperation = $this->isCorrectTypeOfOperation(
      $aircraft->id_type_operation,
      $type_operation
    );

    // available = true && pending_approval = false && show_result = true
    $aircraftIsShown = $this->aircraftIsShown(
      $aircraft->available,
      $aircraft->pending_approval,
      $aircraft->show_results
    );

    $aircraftCapacityIsChecked = $this->aircraftCapacityIsChecked(
      $aircraft->id_type_operation,
      $aircraft->capacity,
      $aircraft->num_passengers
    );

    //distancia desde avion a aeropuerto
    //ejemlo desde venezuela a japon, avion esta en rango de venezuela o japon?
    $aircraftIsWithinRange = $this->aircraftIsWithinRange(
      $legs, //array de aeropuertos por los que voy a pasar
      $aircraft->range_value, // Aircraft - Aircraft_model
      $base_to_departure,
      $base_from_arrival,
      $ENV->STOPS_AIRCRAFT_RANGE //Globals
    );

    $aircraftRangeIsOk = $this->aircraftRangeIsOk(
      $type_operation,
      $aircraft->range_value, // Aircraft - Aircraft_model
      $legs, //[{arrive_airport, departure_airport, distance, date}]
      $ENV->TECHNICAL_STOPS, //GLOBAL
      $ENV->RANGE_CORRECTION //GLOBAL
    );

    //
    $response = $isCorrectTypeOfOperation && $aircraftIsShown && $aircraftCapacityIsChecked && $aircraftIsWithinRange && $aircraftRangeIsOk;

    return $response;
  }

  /**
   * Check Aircraft Type operaation
   * @param integer $flight_operation
   * @param integer $typeOpetype_operationration
   * @return boolean
   */
  public function isCorrectTypeOfOperation($aircraft_operation, $flight_operation)
  {
    $response = false;

    if ($flight_operation === 1 && ($aircraft_operation >= 1 && $aircraft_operation <= 3)) { //charter
      $response = true;
    } else if ($flight_operation === 3 && ($aircraft_operation >= 2 && $aircraft_operation <= 5)) { //medico
      $response = true;
    }
    return $response;
  }

  /**
   * check Aircraft avaliable
   * @param boolean $available
   * @param boolean $pending_approval
   * @param boolean $show_result
   * @return boolean
   */
  private function aircraftIsShown($available, $pending_approval, $show_result)
  {
    $response = false;
    if ($available == true && $pending_approval == false && $show_result == true) {
      $response = true;
    }
    return $response;
  }

  /**
   * check Aircraft avaliable
   * @param int $typeOperation
   * @param int $aircraft_capacity
   * @param int $num_passengers
   * @return boolean
   */
  private function aircraftCapacityIsChecked($typeOperation, $aircraft_capacity, $num_passengers)
  {
    $response = false;
    // ($typeOperation === 1 || $typeOperation === 2 || $typeOperation === 3) &&
    if ($aircraft_capacity >= $num_passengers) {
      $response = true;
    }
    return $response;
  }

  /**
   * check Aircraft range
   * @param array $legs
   * @param int $aircraft_range
   * @param float|int $base_to_departure
   * @param float|int $base_from_arrival
   * @param int $STOPS_AIRCRAFT_RANGE
   * @return boolean
   */
  public function aircraftIsWithinRange(
    $legs,
    $aircraft_range,
    $base_to_departure,
    $base_from_arrival,
    $STOPS_AIRCRAFT_RANGE
  ) {
    $response = false;
    foreach ($legs as $leg) {
      $distance = $leg['distance'] + $base_from_arrival + $base_to_departure;
      if ($distance < ($aircraft_range * $STOPS_AIRCRAFT_RANGE)) {
        $response = true;
        break;
      }
    }
    return $response;
  }

  /**
   * check Aircraft range is ok
   * @param integer $typeOperation
   * @param integer $aircraft_range
   * @param integer $distance_arrival_departure
   * @param array $legs
   * @param integer $TECHNICAL_STOPS
   * @param float $RANGE_CORRECTION
   * @return boolean
   */
  private function aircraftRangeIsOk($flight_operation, $aircraft_range, $legs, $TECHNICAL_STOPS, $RANGE_CORRECTION)
  {
    $response = false;
    if ($flight_operation === 2) { //medico
      $TECHNICAL_STOPS += 1;
    }
    foreach ($legs as $leg) {
      if ($leg['distance'] <= $aircraft_range * $RANGE_CORRECTION * $TECHNICAL_STOPS) {
        $response = true;
        break;
      }
    }
    return $response;
  }

  //TODO Priorización de aeropuertos
  /**
   * check Aircraft range is ok
   * @param integer $aircrafts
   * @return array
   */
  public function findAircraftsAndAirports($aircrafts)
  {
    return [];
  }

  /**
   * Assign Suitable Airport
   * @param array $legs
   * @param array $base_latitude
   * @param bool $base_longitude
   * @param object $ENV
   * @return array
   */
  public function assignSuitableAirport(
    $legs,
    $base_latitude,
    $base_longitude,
    $ENV
  ) {
    $response = false;
    $__debug = [];

    foreach ($legs as $leg) {
      $distance_to_base  = $this->getDistance(
        $leg['arrive_airport']['latitude_deg'],
        $leg['arrive_airport']['longitude_deg'],
        $base_latitude,
        $base_longitude
      );
      // $response = $distance_to_base <= $ENV->MARGIN_RANGE;
      $__debug[] = [
        'distance' => $distance_to_base
      ];
      // $in_range_departure = $leg['departure_airport'] <= $ENV->MARGIN_RANGE;
      // $in_range_arrive    = $leg['arrive_airport'] <= $ENV->MARGIN_RANGE;
      // if ($in_range_departure || $in_range_arrive) {
      //   $response = true;
      // }
    }
    // // $legs[0]['departure_airport']
    // //$ENV->MARGIN_RANGE
    return $legs;
  }


  /**
   * check if Airport is suitable for aircraft
   * @param object $airports_runaways
   * @param object $aircraft
   * @param bool $aircraft
   * @param object $ENV
   * @return bool
   */
  public function airportIsSuitableForAircraft($airports_runaways, $aircraft, $prohibided, $ENV)
  {
    $response = false;
    foreach ($airports_runaways as $runaway) {
      // return $runaway;
      $elevation_correction  = $runaway['le_elevation_ft'] * $ENV->ELEVATION_CORRECTION_COEFICIENT;
      $length_runaway = $runaway['lenght_ft'] >= ($aircraft->required_rwy_length * $elevation_correction);
      $width_runaway = $runaway['width_ft'] >= $aircraft->required_rwy_width;
      $closed = $runaway['closed']; //? cerrado = false, abierto = true
      $elevation = $runaway['le_elevation_ft'] <= $aircraft->max_elev_isa;

      if ($length_runaway && $width_runaway && !$closed && $elevation && !$prohibided) {

        switch ($aircraft->required_rwy_sfc) {
          case 'soft':
            if (
              str_contains($runaway['surface'], "GR")  ||
              str_contains($runaway['surface'], "CON") ||
              str_contains($runaway['surface'], "ASP") ||
              str_contains($runaway['surface'], "MIX")
            ) {
              $response = true;
            }
            break;
          case 'hard':
            if (
              str_contains($runaway['surface'], "CON") && !str_contains($runaway['surface'], "GR") ||
              str_contains($runaway['surface'], "ASP") && !str_contains($runaway['surface'], "GR") ||
              str_contains($runaway['surface'], "MIX") && !str_contains($runaway['surface'], "GR")
            ) {
              $response = true;
            }
            break;
          default:
            $response = false;
            break;
        }

        //todo Para helicopteros (tienen característica para diferenciarlos), debe considerar helipuertos. Son de aicraft_group 11 preguntar por esto a Franco
      } else {
        return $response;
      }
    }
    return $response;
  }


  /**
   * check airport forbidden
   * @param array $legs
   * @param array $blacklist
   * @return bool
   */
  public function airportIsForbidden($legs, $blacklist)
  {
    $response = false;
    foreach ($blacklist as $airport_prohibited) { //? looping aeropuertos prohibidos

      foreach ($legs as $leg) {
        if ($airport_prohibited->id_airport === $leg['arrive_airport'] || $airport_prohibited->id_airport === $leg['departure_airport']) {
          $response = true;
        }
      }
    }
    return $response;
  }

  //!Distancias
  /**
   * Convert KM to NM
   * @param float $aircraft_units_km
   * @return boolean
   */
  public function convertKmToNm($aircraft_units_km)
  {
    return $aircraft_units_km / 1.852;
  }

  /**
   * Convert Nm to KM
   * @param float $aircraft_units_nm
   * @return boolean
   */
  public function convertNmToKm($aircraft_units_nm)
  {
    return $aircraft_units_nm * 1.852;
  }

  //!Cálculo de distancias
  /**
   * get calcular la distancia total
   * @param int $total_distance
   * @param array $passengers
   * @param integer $type_operation
   * @param integer $flight_type
   * @param float $DISTANCE_CORRECTION_COEFFICIENT
   * @return float
   */
  public function calculateTotalDistance($partial_distance, $passengers, $type_operation, $flight_type, $DISTANCE_CORRECTION_COEFFICIENT)
  {
    $total_distance = 0;
    $twoAdults = false;
    $twoChilds = false;
    if (count($passengers) === 2) {
      $twoAdults = ($passengers[0]['type'] == 'adult' && $passengers[1]['type'] == 'adult') && ($type_operation == 2 || $type_operation == 4);
      $twoChilds = ($passengers[0]['type'] == 'child' && $passengers[1]['type'] == 'child') && ($type_operation == 2 || $type_operation == 4);
    }

    if ($twoAdults || $flight_type == 2) { //dos adultos y una camilla
      $total_distance = $partial_distance * 2; //TODO falta añadir a su base
    } else if ($twoChilds) { //dos niños y una camilla
      $total_distance = $partial_distance;
    } else {
      $total_distance = $partial_distance;
    }
    $total_distance = $total_distance * $DISTANCE_CORRECTION_COEFFICIENT;
    return $total_distance;
  }

  /**
   * get calcular la distancia considerada
   * considered_distance es la distancia total - la distancia de cada tramo debajo de min_trip_lenght
   * @param array $legs
   * @return double
   */
  public function calculateConsideredDistance($aircraft, $legs, $total_distance)
  {
    $considered_distance = $total_distance;

    foreach ($legs as $leg) {
      if ($leg['distance'] < $aircraft->min_trip_lenght) {
        $considered_distance -= $leg['distance'];
      }
    }
    return round($considered_distance, 2);
  }

  /**
   * get below minimum length trips
   * @param object $aircraft
   * @param array $legs
   * @return float
   */
  public function calculatebelowMinimumLenghtTrips($aircraft, $legs)
  {
    $below_min_lenght_trips = 0;

    foreach ($legs as $leg) {
      if ($leg['distance'] <= $aircraft->min_trip_lenght) {
        $below_min_lenght_trips += 1;
      }
    }
    return $below_min_lenght_trips;
  }


  /**
   * Calcular la distancia promedio por leg
   * @param array $legs
   * @param integer $considered_distance
   * @param integer $below_min_lenght_trips
   * @return double
   */
  public function calculateAverageLegDistance($legs, $considered_distance, $below_min_lenght_trips)
  {

    if (count($legs) > $below_min_lenght_trips) {
      $AverageLegDistance = $considered_distance  / (count($legs)  - $below_min_lenght_trips);
    } else {
      $AverageLegDistance = 1;
    }
    return $AverageLegDistance;
  }

  //!Cálculo de layover
  /**
   * check is national destination
   * @param integer $airport_country
   * @param integer $aircraft_country
   * @return boolean
   */
  public function isNationalDestination($airport_country, $aircraft_country)
  {
    $response = false;
    if ($airport_country === $aircraft_country) {
      $response = true;
    }
    return $response;
  }

  /**
   * check Average leg distance
   * @param object $aircraft
   * @param object $legs
   * @param integer $DISTANCE_1
   * @return mixed
   */
  public function calculateTotalLayOverCost($aircraft, $flight, $DISTANCE_1)
  {
    $value_layover = 0;
    $airport_parking = 0;
    $total_layover = 0;
    $staying_airport_cost = 0;
    $aircraft_return_cost = 0;
    $aircraft_type = $aircraft->type_aircraft_groups;
    $departure_time = 0;
    $departure_date = 0;
    $i = 0;

    foreach ($flight->legs as $leg) {

      //get departure time
      if (array_key_exists($i + 1, $flight->legs)) {
        $current_date = new \DateTime($leg['date']);
        $next_date = new \DateTime($flight->legs[$i + 1]['date']);
        $diff = $current_date->diff($next_date);

        $departure_time = abs($leg['arrival_time'] - $flight->legs[$i + 1]['arrival_time']); //positive
        $departure_date = $diff->days; //positive
      }

      if ($departure_time === 12 && $flight->total_distance > $DISTANCE_1 && $this->isNationalDestination($leg['arrive_airport'], $aircraft->id_country)) {
        $value_layover = $aircraft->national_layovers_cost;
      } else {
        $value_layover = $aircraft->international_layovers_cost;
      }

      if ($aircraft_type < 5 || $aircraft_type > 10) {
        $airport_parking = $aircraft->airport_layover_1;
      } else if ($aircraft_type === 6 || $aircraft_type === 7 || $aircraft_type === 8) {
        $airport_parking = $aircraft->airport_layover_2;
      } else if ($aircraft_type === 9 || $aircraft_type === 10) {
        $airport_parking = $aircraft->airport_layover_3;
      }

      $staying_airport_cost = $departure_date * ($value_layover + $airport_parking);

      switch ($flight->average_distance) {
        case $flight->average_distance < $aircraft->distance_th_1:
          $price = $aircraft->price_1;
          break;
        case $flight->average_distance < $aircraft->distance_th_2:
          $price = $aircraft->price_2;
          break;

        case $flight->average_distance < $aircraft->distance_th_3:
          $price = $aircraft->price_3;
          break;

        case $flight->average_distance < $aircraft->distance_th_4:
          $price = $aircraft->price_4;
          break;

        default:
          $price = $aircraft->price_5;
          break;
      }

      $aircraft_return_cost = $flight->travel_distances->base_from_arrival * 2 * $price;

      if (
        $staying_airport_cost < $aircraft_return_cost
      ) {
        $total_layover += $staying_airport_cost;
      } else {
        $total_layover += $aircraft_return_cost;
      }
      $i += 1;
    }
    return $aircraft_return_cost;
  }

  //!Cálculo vuelo médico
  /**
   * calculate medical cost
   * @param object $aircraft
   * @param integer $total_distance
   * @param integer $patient_passengers
   * @return boolean
   */
  public function calculateMedicalCost($aircraft, $total_distance, $patient_passengers)
  {
    $medical_cost = 0;
    if ($total_distance < $aircraft->medical_cost_th) {
      $medical_cost = $patient_passengers * $aircraft->medical_cost_1;
    } else {
      $medical_cost = $patient_passengers * $aircraft->medical_cost_2;
    }
    return $medical_cost;
  }

  //!Cálculo precio de vuelo
  /**
   * calculate dynamics rates
   * @param string $flight_date ("2020-03-29")
   * @param float $RATE_REMAINING_DAYS
   * @return int|float 
   */
  public function calculateDynamicRate($flight_date, $RATE_REMAINING_DAYS)
  {
    $current_date = date("Y-m-d");
    $s = strtotime($current_date) - strtotime($flight_date);
    $d = intval($s / 86400);
    $days_to_flight = $d;

    if ($days_to_flight > 45) {
      $response = 0;
    } else {
      $response = (45 - $days_to_flight) * $RATE_REMAINING_DAYS; //(MODIF, $tarifaDiasRestantes)
    }
    return $response;
  }

  /**
   * Calculate Supercharge Cost
   * @param string $factor_correction 
   * @param string $cost 
   * @return int|float 
   */
  public function calculateSurchargeCost($factor_correction, $cost)
  {
    if ($factor_correction) {
      $response = $factor_correction * $cost;
    }
    return $response;
  }

  /**
   * Calculate Supercharge Cost
   * @param object $aircraft 
   * @param object $flight 
   * @param int $DISTANCE_1 
   * @param int $patient_passengers (Optional)
   * @return int|float quoted_operator_price
   */
  public function calculateQuotedOperatorCost($aircraft, $flight, $DISTANCE_1, $patient_passengers = 0, $SUPERCHARGE_FACTOR_CORRECTION, $type_operation)
  {

    $distance_cost = 0;
    $hours_cost = 0;
    $response = 0;
    $below_min_trip_lenght = $this->calculatebelowMinimumLenghtTrips($aircraft, $flight->legs);
    if ($aircraft->unit_used_km == 1) { //km
      if ($flight->total_distance < $aircraft->min_distance) {
        $distance_cost = $aircraft->min_distance * $aircraft->price_1;
      } else {
        if ($aircraft->has_segmented_prices == 1) {

          switch ($flight->average_distance) {
            case $flight->average_distance < $aircraft->distance_th_2:
              $distance_cost = $flight->total_distance * $aircraft->price_2;
              break;

            case $flight->average_distance < $aircraft->distance_th_3:
              $distance_cost = $flight->total_distance * $aircraft->price_3;
              break;

            case $flight->average_distance < $aircraft->distance_th_4:
              $distance_cost = $flight->total_distance * $aircraft->price_4;
              break;

            default:
              $distance_cost = $flight->total_distance * $aircraft->price_5;
              break;
          }
        } else {
          $distance_cost = $flight->total_distance * $aircraft->aircraft_price_5;
        }
        if ($below_min_trip_lenght > 0) {
          $distance_cost = $below_min_trip_lenght * $aircraft->min_trip_price;
        }
      }
      $total = $distance_cost + $this->calculateTotalLayOverCost($aircraft, $flight, $DISTANCE_1) + $this->calculateSurchargeCost($SUPERCHARGE_FACTOR_CORRECTION, $distance_cost);
      if ($type_operation === 3) {
        $total += $this->calculateMedicalCost($aircraft, $flight->total_distance, $patient_passengers);
      }
      $response = $total;
      //
      // DEBUG
    } else { //hours
      // $hours_cost = (($flight->total_distance / $aircraft->average_speed) + 0.33) * $hora_vuelo;
      if ($flight->considered_distance < $aircraft->min_distance) {
        // $distance_cost = $aircraft->min_distance * $aircraft->price_1;
        $distance_cost = $aircraft->min_distance * $aircraft->price_1;
        //*FRANCO
        //aircraft->price_1 es un valor por hora, 
        //pasar a horas según 
        //aircraft_average_speed_1 + 0.33 por leg. 
        //Lo mismo para todos los CASE de abajo.
      } else {
        switch ($flight->average_distance) {
          case $flight->average_distance < $aircraft->distance_th_1:
            $distance_cost = $flight->total_distance * $aircraft->price_1;
            break;
          case $flight->average_distance < $aircraft->distance_th_2:
            $distance_cost = $flight->total_distance * $aircraft->price_2;
            break;
          case $flight->average_distance < $aircraft->distance_th_3:
            $distance_cost = $flight->total_distance * $aircraft->price_3;
            break;
          case $flight->average_distance < $aircraft->distance_th_4:
            $distance_cost = $flight->total_distance * $aircraft->price_4;
            break;
          default:
            $distance_cost = $flight->total_distance * $aircraft->price_5;
            break;
        }
      }
      if ($aircraft->min_trip_lenght > 0) {
        $hours_cost += $aircraft->min_trip_lenght * $aircraft->min_trip_price;
      }
      $total = $hours_cost + $this->calculateTotalLayOverCost($aircraft, $flight, $DISTANCE_1) + $this->calculateSurchargeCost($SUPERCHARGE_FACTOR_CORRECTION, $hours_cost);
      if ($type_operation === 3) {
        $total += $this->calculateMedicalCost($aircraft, $flight->total_distance, $patient_passengers);
      }
      $response = $total;
    }
    return $response;
  }

  //!Cálculo Impuestos
  /**
   * check national flight
   * @param object $aircraft 
   * @param array $airports 
   * @return int|float 
   */
  public function isNationalFlight($aircraft, $airports)
  {
    foreach ($airports as $airport) {
      if ($airport['arrive_airport']['id_country'] === $aircraft->id_country && $airport['departure_airport']['id_country'] === $aircraft->id_country) {
        $response = true;
      } else {
        $response = false;
      }
    }
    return $response;
  }

  /**
   * taxes calculate quoted_operator_taxes || operator_taxes
   * @param object $aircraft 
   * @param array $airports 
   * @return int|float 
   */
  public function calculateTaxes($aircraft, $airports, $flight)
  {
    $taxes = 0;

    if ($this->isNationalFlight($aircraft, $airports)) {
      $taxes += $flight->quoted_operator_price * ($aircraft->national_taxes / 100);
    } else {
      $taxes += $flight->quoted_operator_price * ($aircraft->international_taxes / 100);
    }
    return $taxes;
  }

  //!Tasas
  /**
   * Rates calculate
   * @param array $legs 
   * @param integer $passengers 
   * @param integer $flight_type 
   * @return int|float 
   */
  public function rates($legs, $passengers, $flight_type)
  {
    $rates = 0;
    $handling_services = 0;
    foreach ($legs as $leg) {
      $rates += $leg['arrive_airport']['departure_taxes'] + $leg['departure_airport']['arrival_taxes'];
      $handling_services += $leg['arrive_airport']['handling_services'] + $leg['departure_airport']['handling_services'];
    }
    $rates = $flight_type === 2 ? $rates * 2 : $rates;
    $total_rates = ($rates * $passengers) + $handling_services;

    return $total_rates;
  }

  //!Calculo Quoted_margin
  /**
   * Rates calculate
   * @param float $quoted_operator_price 
   * @param integer $user_level
   * @param string $flight_date ("2020-03-29")
   * @param object $ENV 
   * @return int|float 
   */
  public function calculateQuotedMargin($quoted_operator_price, $user_level = 1, $flight_date, $ENV)
  {
    // quoted_margin= ( quoted_operator_price * calculateDynamicRate() ) + ( quoted_operator_price * global.rate_coeficient_1 ) + fixed_value_1 

    $quoted_margin = 0;
    if ($user_level == 1) { //default
      // (($quoted_operator_price + ($quoted_operator_price * $this->calculateDynamicRate($flight_date, $ENV->RATE_REMAINING_DAYS))) * $ENV->RATE_COEFFICIENT) + $ENV->FIXED_VALUE;
      $quoted_margin = ($quoted_operator_price * $this->calculateDynamicRate($flight_date, $ENV->RATE_REMAINING_DAYS)) + ($quoted_operator_price * $ENV->RATE_COEFFICIENT) + $ENV->FIXED_VALUE;
    } else if ($user_level == 2) {
      $quoted_margin = ($quoted_operator_price + ($quoted_operator_price * ($this->calculateDynamicRate($flight_date, $ENV->RATE_REMAINING_DAYS) / $ENV->DYNAMIC_RATE_COEFICIENT_RATE_LEVEL_2))) * $ENV->RATE_COEFFICIENT + $ENV->FIXED_VALUE_2;
    } elseif ($user_level == 3) {
      $quoted_margin = ($quoted_operator_price + ($quoted_operator_price * $this->calculateDynamicRate($flight_date, $ENV->RATE_REMAINING_DAYS) / $ENV->DYNAMIC_RATE_COEFICIENT_RATE_LEVEL_3)) * $ENV->RATE_COEFFICIENT + $ENV->FIXED_VALUE_3;
    }
    return $quoted_margin;
  }

  //!Calculo valor total
  /**
   * Rates calculate
   * @param float $quoted_operator_taxes 
   * @param float $quoted_airport_taxes 
   * @param float $quoted_operator_price 
   * @param float $quoted_margins 
   * @return float 
   */
  public function totalChargedToClient(
    $quoted_operator_taxes,
    $quoted_airport_taxes,
    $quoted_operator_price,
    $quoted_margins
  ) {
    $total_charged_to_client = $quoted_operator_taxes + $quoted_airport_taxes + $quoted_operator_price + $quoted_margins;
    return $total_charged_to_client;
  }

  public function calculateFlightTime($aircraft, $distance)
  {
    $flight_time = 0;
    switch ($distance->average_distance) {
      case $distance->average_distance < $aircraft->distance_th_1:
        $flight_time = 0.33 + ($distance->total_distance / $aircraft->avg_speed_1);
        break;
      case $distance->average_distance < $aircraft->distance_th_2:
        $flight_time = 0.33 + ($distance->total_distance / $aircraft->avg_speed_2);
        break;
      case $distance->average_distance < $aircraft->distance_th_3:
        $flight_time = 0.33 + ($distance->total_distance / $aircraft->avg_speed_3);
        break;
      case $distance->average_distance < $aircraft->distance_th_4:
        $flight_time = 0.33 + ($distance->total_distance / $aircraft->avg_speed_4);
        break;

      default:
        $flight_time = 0.33 + ($distance->total_distance / $aircraft->avg_speed_5);
        break;
    }
    return $flight_time;
  }
  /**
   * get km between two coordinates
   * Optimized algorithm from http://www.codexworld.com
   *
   * @param float $latitudeFrom
   * @param float $longitudeFrom
   * @param float $latitudeTo
   * @param float $longitudeTo
   *
   * @return float [km]
   */
  function getDistance(
    $latitudeFrom,
    $longitudeFrom,
    $latitudeTo,
    $longitudeTo,
    $round = true
  ) {
    $rad = M_PI / 180;
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad)
      * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
      * cos($latitudeTo * $rad) * cos($theta * $rad);

    // $raw = acos($dist) / $rad * 60 *  1.85;
    // if ($round) {
    //   $distance = number_format($raw, 3);
    // } else {
    //   $distance = $raw;
    // }
    return acos($dist) / $rad * 60 *  1.85;
  }
}
