## Api Rest VuelosApp (jirehPro) Laravel 7.3

A continuación se describen los pasos para hacer la instalación del proyecto en los entornos de desarrollo.

-Instalacion inicial
composer install
cp .env.example .env (y configurar el archivo .env)
php artisan key:generate
php artisan jwt:secret
php artisan cache:clear
php artisan config:clear

-Levantar la api
php artisan serve

-Configurar la base de datos
php artisan migrate:fresh --seed
composer dump-autoload (si los seeder dan problemas)

-Resetear cache de la app
php artisan config:cache
